import { BrowserRouter as Router, Route } from "react-router-dom";
import React from "react";

import "./App.scss";
import "./scss/main.scss";

import "appkit-react/style/appkit-react.default.css";
import "bootstrap/dist/css/bootstrap.css";

import Header from "./components/header/header";
import Navigation from "./components/left-navigation/navigation";
import Dashboard from "./components/dashboard/dashboard";
import FileUpload from "./components/file-upload/fileupload";
import Transactionlist from "./components/transaction-list/transactionlist";
import Remediation from "./components/remediation/remediation";
import RemediationList from "./components/remediation-list/remediationlist";
import Disclosure from "./components/data-disclosure/disclosure";
import Breadcrum from "./common/breadcrumb/breadcrumb";
import Followpopup from "./components/popup/followpopup";

function App() {
  return (
    <Router>
      <Header />
      <div className="row">
        <div className="col-sm-2">
          <Navigation />
        </div>
        <div className="col-sm-10">
          <Route exact path="/" component={Dashboard} />
          <Route exact path="/fileupload" component={FileUpload}/>
          <Route exact path="/followpopup" component={Followpopup}/>
          <Route exact path="/remediation" component={Remediation} />
          <Route exact path="/remediationlist" component={RemediationList} />
          <Route exact path="/transactionlist" component={Transactionlist} />
          <Route exact path="/disclosure" component={Disclosure} />
          <Route exact path="/bread" component={Breadcrum} />
        </div>
      </div>
    </Router>
  );
}

export default App;
