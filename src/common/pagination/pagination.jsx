import React from "react";
import "../pagination/pagination.scss";
import { Pagination } from "appkit-react";

class PaginationList extends React.Component {
  render() {
    return (
      <React.Fragment>
        <div className="pagination-aligned">
          <Pagination total={300} />
        </div>
      </React.Fragment>
    );
  }
}

export default PaginationList;
