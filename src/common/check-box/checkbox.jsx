import React from "react";
import { Checkbox } from "appkit-react";

class CheckboxGroup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      values: []
    };
  }

  handleChange = value => event => {
    let { values } = this.state;
    if (values.indexOf(value) > -1) {
      values = values.filter(item => item !== value);
    } else {
      values.push(value);
    }
    this.setState({ values });
  };

  render() {
    return (
      <div style={{ display: "flex", flexDirection: "column" }}>
        {["item 1", "item 2", "item 3", "item 4"].map((value, key) => (
          <Checkbox
            key={key}
            checked={this.state.values.indexOf(value) > -1}
            value={value}
            onChange={this.handleChange(value)}
            className="a-mb-20"
          >
            {value}
          </Checkbox>
        ))}
      </div>
    );
  }
}

export default CheckboxGroup;
