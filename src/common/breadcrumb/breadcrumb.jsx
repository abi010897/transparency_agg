import React from "react";
import { Breadcrumb } from "appkit-react";
import "./breadcrumb.scss";

const Breadcrum = props => {
  return (
    <div className="breadcrumb-story-container">
      <Breadcrumb items={props.items} />
    </div>
  );
};

export default Breadcrum;
