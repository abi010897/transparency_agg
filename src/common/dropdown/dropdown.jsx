import React, { Component } from "react";
import { Select, SelectOption } from "appkit-react";
import "../dropdown/dropdown.scss";

class Dropdown extends Component {
  constructor() {
    super();
  }

  render() {
    const containerStyle = { display: "flex", alignItems: "center" };
    return (
      <div className="dropdown-section">
        <Select defaultValue={1} onSelect={e => console.log("onSelect", e)}>
          <SelectOption key={1} value={1}>
            Option one
          </SelectOption>
          <SelectOption key={2} value={2}>
            Option two
          </SelectOption>
          <SelectOption key={3} value={3}>
            Option three
          </SelectOption>
        </Select>
      </div>
    );
  }
}

export default Dropdown;
