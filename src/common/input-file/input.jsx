import React, { Component } from "react";
import { Input } from "appkit-react";

class InputFile extends Component {
  render() {
    return (
      <div className="input-content">
        <Input inputBoxSize="sm" label="LABEL" />
      </div>
    );
  }
}

export default InputFile;
