import React, { Component } from "react";
import "appkit-react/style/appkit-react.default.css";
import fileUpload from "../../images/fileupload.png";
import "./uploadfile.scss";
// import Loader from "../loader/loader";

class UploadFile extends Component {
  constructor() {
    super();
    this.state = {
      files: [],
      file: null,
      uploadFiles: [],
      loader: false,
      addClass: "upload-loader-align",
      numberOfFiles: 0
    };
  }

  myFunction = e => {
    const file = Array.from(e.target.files);

    var fileDetails = [];
    for (var i = 0; i < file.length; i++) {
      var payload = {
        filename: file[i].name,
        size: file[i].size
      };
      fileDetails.push(payload);
    }
    
    const url = `${process.env.REACT_APP_API_URL}/upload`;
    const formData = new FormData();
   
    file &&
      file.map((eachFile, index) =>
        formData.append(`file${index}`, file[index])
      );

    
    fetch(url, {
      method: "POST",
      body: formData
    })
      .then(res => res.json())
      .then(res => {
        var data = res.data;
       
        this.setState({ files: data, numberOfFiles: data.length });
        if (data.length !== 0) {
          localStorage.setItem("fileNames", JSON.stringify(this.state.files));
         
          localStorage.setItem("filesLength", this.state.numberOfFiles);
        } else {
          localStorage.setItem("fileNames", [
            { filename: "There are no file uploades" }
          ]);
        }
      });
  };
  loaderDisplay = () => {
    return this.setState({ loader: true });
  };

  render() {
    const contentText =
      "orem ipsum dolor sit amet, consectetur adipisicing elit. Iusto possimus at a cum saepe molestias modi illo facere ducimus voluptatibus praesentium deleniti fugiat.";
    return (
      <div className="uploading-data">
        <h2 className="File-upload-name">File Upload</h2>
        <p className="upload-content">Use this space to upload new files. </p>
        <div className="row">
          <div className="col-sm-6">
            <div className="upload-img">
              <img className="upload" src={fileUpload} alt=""></img>
              <div
                className={
                  this.state.loader === true ? "loder" : "display-none"
                }
              >
                {/* <Loader addClass={this.state.addClass} /> */}
              </div>

              <input
                type="file"
                id="myFile"
                onChange={this.myFunction}
                onClick={this.loaderDisplay}
                encType="multipart/form-data"
                required
                multiple
              ></input>
            </div>
          </div>
          <div className="col-sm-6">
            <p
              className={
                this.state.numberOfFiles !== 0 ? "text-size" : "display-none"
              }
            >
              <span className="count">{this.state.numberOfFiles}</span> files
              uploaded, <span className="count">200</span> records total
            </p>

            <ul id="demo" className="file-name">
              {this.state.files.map(el => (
                <li key={el.filename}>
                  {" "}
                  <span className="xls ">XLS</span>
                  {el.filename}
                </li>
              ))}
            </ul>
          </div>
        </div>
        <div></div>
      </div>
    );
  }
}
export default UploadFile;
