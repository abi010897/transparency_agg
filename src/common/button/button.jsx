import React, { Component } from "react";
import { Button } from "appkit-react";

class PrimaryButton extends Component {
  render() {
    return (
      <div className="btn-section">
        <Button>PRIMARY SM</Button>
      </div>
    );
  }
}

export default PrimaryButton;
