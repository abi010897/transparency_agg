import React from "react";
import "../loader/loader.scss";
import CircularProgress from "@material-ui/core/CircularProgress";
// import {LocalLoader} from "appkit-react";

class Loader extends React.Component {
  render() {
    return (
      <React.Fragment>
        <div className="loader-align">
          <CircularProgress classes={{ circle: "loader-color" }} />
        </div>

        {/* <div>
          <LocalLoader style={{ borderLeftColor: "#d04a02" }} />
        </div> */}

        {/* <div className="col-12 d-table">
          <div className="d-flex justify-content-center">
            <div
              className="a-p-10"
              className={
                this.props.addClass === undefined
                  ? "loader-align"
                  : `${this.props.addClass} loader-align`
              }
            >
              <div className="a-loading a-primary loading-large"></div>
            </div>
          </div>
        </div> */}
      </React.Fragment>
    );
  }
}

export default Loader;
