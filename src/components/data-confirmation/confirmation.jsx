import React, { Component } from "react";
import "./confirmation.scss";
import Modal from "react-bootstrap/Modal";
import mod from "../../images/modal.PNG";
import { NavLink } from "react-router-dom";

const initialState = {
  isOpen: false,
  value: "defaultvalue"
};
class Confirmation extends Component {
  constructor() {
    super();
    this.state = initialState;
    this.state = {
      dataUploadFilesName: [localStorage.getItem("upload")],
      filesLength: localStorage.getItem("filesLength") || 0,
      dataCanciliation: [
        {
          FileName: "111-124-CN-41",
          Status: "select all"
        },
        {
          FileName: "111-124-CN-61",
          Status: "REM-111-1101"
        },
        {
          FileName: "111-124-CN-653",
          Status: "Pending Approval"
        },
        {
          FileName: "111-124-CN-4",
          Status: "Pending Approval"
        }
      ],
      dataMapping: [
        {
          FileName: "Recepient -> Recepient Type",
          Status: "select all"
        },
        {
          FileName: "Activity Type -> CDF Activity Type",
          Status: "Pending Approval"
        },
        {
          FileName: "Currency Type -> ToV Currency",
          Status: "REM-111-1101"
        },
        {
          FileName: "Location -> Transaction Location",
          Status: "Pending Approval"
        },
        {
          FileName: "Name 1 -> First Name",
          Status: "Pending Approval"
        },
        {
          FileName: "Name 2 -> Last Name",
          Status: "Pending Approval"
        },
        {
          FileName: "Research Rule -> Research Rate",
          Status: "Pending Approval"
        },

        {
          FileName: "Email -> Email Address",
          Status: "Pending Approval"
        }
      ],
      bulkData: [
        {
          transaction: "111-124-CN-92964",
          fieldUpdate: "Currency Type",
          updateValue: "CAD (Canadian Dollars)"
        },
        {
          transaction: "113-551-SAP-12321",
          fieldUpdate: "Currency Type",
          updateValue: "CAD (Canadian Dollars)"
        },
        {
          transaction: "113-551-SAP-12392",
          fieldUpdate: "Currency Type",
          updateValue: "CAD (Canadian Dollars)"
        }
      ]
    };
  }

  toggleModalClose = () => {
    this.setState(initialState);
  };

  componentWillMount() {
    this.setState({
      isOpen: !this.state.isOpen
    });
    var uploadFileNames = localStorage.getItem("fileNames");
    var parseData = JSON.parse(uploadFileNames);

    return this.setState({
      dataUploadFilesName: parseData || [
        { filename: "There are no file uploades" }
      ]
    });
  }

  tablerow() {
    return (
      <ul>
        {this.state.dataUploadFilesName.map(el => (
          <li key={el}>{el}</li>
        ))}
      </ul>
    );
  }

  render() {
    return (
      <div className="confirmation">
        <Modal show={this.state.isOpen} onClose={this.toggleModalClose}>
          <Modal.Header>
            <Modal.Title>
              <p className="confirm-text">
                Remove 3 duplicates and keep 1 record?
              </p>{" "}
              <br />
              <p className="modal-text">
                Once you confirm, this action can not been undone .{" "}
              </p>
              <div
                className="close-btn"
                variant="secondary"
                onClick={this.toggleModalClose}
              >
                X
              </div>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <img src={mod} alt="" width="96%" />
          </Modal.Body>
          <Modal.Footer>
            <p variant="secondary" onClick={this.toggleModalClose}>
              CANCEL
            </p>
            <p variant="primary" onClick={this.toggleModalClose}>
              CONFIRM
            </p>
          </Modal.Footer>
        </Modal>
        <div className="data-confirmation">
          <h5 className="font-weight">Upload File</h5>
          <div className="row">
            <div className="database-details col-sm-9">
              <p className="text-size">
                <span className="count">{this.state.filesLength}</span> files
                uploaded, <span className="count">200</span> records total
              </p>
            </div>
            <div className="hide-details col-sm-3">
              <span className="text-size hide">Hide Details </span>
              <span className="text-size edit">
                <NavLink to="/fileupload" className="open-task">
                  Edit &nbsp;&nbsp;
                  <i className="fa fa-pencil-square-o" aria-hidden="true"></i>
                </NavLink>
              </span>
            </div>
          </div>
          <div className="file-name">
            <ul className="row">
              {this.state.dataUploadFilesName.map((el, i) => (
                <li className="col-sm-6" key={el.filename}>
                  <span className="xls ">XLS</span>
                  {el.filename}
                </li>
              ))}
            </ul>
          </div>
        </div>

        <div className="data-confirmation">
          <h5 className="font-weight">Data Mapping</h5>
          <div className="row">
            <div className="database-details col-sm-9">
              <p className="text-size">
                <b>{this.state.filesLength}</b> files selected, <b>8</b> fields
                remapped
              </p>
            </div>
            <div className="hide-details col-sm-3">
              <span className="text-size hide">Hide Details</span>
              <span className="text-size edit">
                <NavLink to="/fileupload" className="open-task">
                  Edit&nbsp;&nbsp;
                  <i className="fa fa-pencil-square-o" aria-hidden="true"></i>
                </NavLink>
              </span>
            </div>
          </div>
          <div className="file-name">
            <ul className="row">
              {this.state.dataMapping.map((el, i) => (
                <li className="col-sm-6" key={i}>
                  <span className="xls ">XLS</span>
                  {el.FileName}
                </li>
              ))}
            </ul>
          </div>
        </div>

        <div className="data-confirmation">
          <h5 className="font-weight">Data Reconciliation</h5>
          <div className="row">
            <div className="database-details col-sm-9">
              <p className="text-size">
                <b>4</b> transactions merged
              </p>
            </div>
            <div className="hide-details col-sm-3">
              <span className="text-size hide">Hide Details</span>
              <span className="text-size edit">
                <NavLink to="/fileupload" className="open-task">
                  Edit &nbsp;&nbsp;
                  <i className="fa fa-pencil-square-o" aria-hidden="true"></i>
                </NavLink>
              </span>
            </div>
          </div>
          <div className="file-name">
            <ul className="row">
              {this.state.dataCanciliation.map((el, i) => (
                <li className="col-sm-6" key={i}>
                  <span className="xls ">XLS</span>
                  {el.FileName}
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default Confirmation;
