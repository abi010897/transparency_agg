import React, { Component } from "react";
import Stepper from "../stepper-horiz/stepper";
import "../file-upload/fileupload.scss";
import "../../common/breadcrumb/breadcrumb";

class FileUpload extends Component {
  constructor() {
    super();
    this.state = {
      selectedFile: []
    };
  }

  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {
    this.uploadInput = this.uploadInput || [];

    return (
      <div className="cont-right">
        <div className="back-menu">
          <span>
            <span className="appkiticon icon-home-fill font-14"></span>
            <span className="slash">/</span>
            <span className="dashBoard"> New Data Upload</span>
          </span>
        </div>
        <h2 className="data-upload">New Data Upload Wizard</h2>
        <div className="file-upload">
          <div className="stepper">
            <Stepper />
          </div>
        </div>
      </div>
    );
  }
}

export default FileUpload;
