import React, { Component } from "react";
import { setTimeout } from "timers";
import Search from "../../images/Search.png";
import Breadcrum from "../../common/breadcrumb/breadcrumb";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from "appkit-react";
import Loader from "../../common/loader/loader";
import "./transactionlist.scss";
import axios from "axios";
import _get from "lodash/get";
import { NavLink } from "react-router-dom";
import PaginationList from "../../common/pagination/pagination";

const initialState = {
  isOpen: false,
  value: "defaultvalue"
};

const items = [
  {
    link: "/",
    value: <span className="appkiticon icon-home-fill font-14"></span>
  },
  {
    link: "/remediation",
    value: "Open Tasks"
  },
  {
    link: "",
    value: "Transaction list"
  }
];
class Transactionlist extends Component {
  constructor(props) {
    super(props);

    this.state = {
      initialItems: [],
      items: [],
      filter: [],
      visible: false,
      visiblePopup: false,
      data: [{}],
      updatedValue: {}
    };
    this.filterList = this.filterList.bind(this);
    this.showValue = this.showValue.bind(this);
    let path = this.props.history.location.pathname;
  }

  componentDidMount() {
    if (_get(this.props, "location.state") === "fromTransactionList") {
      axios
        .get(`${process.env.REACT_APP_API_URL}/api/transactionList`)
        .then(res => {
          const data = res.data;
          this.setState({ filter: data.data });
          this.setState({ items: data.data });
        });
      this.setState({ items: this.state.filter });
    } else {
      axios
        .get(`${process.env.REACT_APP_API_URL}/api/update/transactionList`)
        .then(res => {
          const data = res.data;
          this.setState({ filter: data.data });
          this.setState({ items: data.data });
        });
      this.setState({ items: this.state.filter });
    }
  }

  componentDidUpdate(prevProps) {
    if (
      _get(prevProps, "location.pathname") !==
      _get(this.props, "location.pathname")
    ) {
    }
  }

  componentWillMount() {
    setTimeout(() => {
      this.setState({ isLoader: false });
    }, 1000);
    this.setState({
      items: this.state.initialItems,
      isLoader: true
    });
  }

  filterList = e => {
    let total = this.state.filter;
   
    let updateList = total;

    updateList = updateList.filter(item => {
     
      if (e.target.value !== null) {
        return (
          item.Status.toLowerCase().search(e.target.value.toLowerCase()) !== -1
        );
      }
    });
   
    this.setState({
      items: updateList
    });
  };

  upadtedlistdata = event => {
    const name = event.target.name;
    const value = event.target.value;

    this.setState(prevState => ({
      updatedValue: {
        ...prevState.updatedValue,
        [name]: value
      }
    }));
  };

  updateTransaction = FileName => {
    axios
      .put(
        `${process.env.REACT_APP_API_URL}/api/transactionListData/update?filename=${FileName}`,

        {
          data: this.state.updatedValue
        }
      )
      .then(response => {
        this.setState({
          visible: false,
          items: response.data.data,
          updatedValue: {}
        });
      });
  };

  addNewData = () => {
    const constNewData = {
      FileName: "2189",
      Status: "Appian",
      Records: "Ready for Remediation",
      Errors: "Blank or Invalid contry, Blank or invalid submitting country",
      LastUpdate: "HCP",
      report: "done",
      VendorName: "Medical Affairs"
    };

    axios
      .post(`${process.env.REACT_APP_API_URL}/api/transactionListData/save`, {
        data: constNewData
      })
      .then(res => {
        const responseData = res.data;
        this.setState({
          visiblePopup: false,
          items: responseData.data
        });
      });
  };

  deleteList(FileName) {
    axios
      .delete(
        `${process.env.REACT_APP_API_URL}/api/transactionListData/delete?filename=${FileName}`
      )
      .then(res => {
        const newList = res.data;
        this.setState({ items: newList.data });
      });
  }

  getCircleClass(items) {
    return items.report === "inProgress"
      ? "task-circle"
      : items.report === "done"
      ? "task-done"
      : items.report === "Support Requested"
      ? "restricted"
      : "";
  }
  toggleModalClose = () => {
    this.setState(initialState);
  };
  openModal = () => {
    this.setState({ isOpen: !this.state.isOpen });
  };

  showModal = (modalId, fileName) => {
    let emparray = [];

    if (fileName) {
      emparray = this.state.items.filter(
        eachelement => eachelement.FileName === fileName
      );
    }
   
    this.setState({
      visible: true,
      updatedValue: emparray ? emparray[0] : {}
    });
  };

  ShowPopup = () => {
    this.setState({
      visiblePopup: true
    });
  };

  showValue = e => {
    this.setState({
      data: {
        FileName: "2189",
        Status: "Appian",
        Records: "Ready for Remediation",
        Errors: "Blank or Invalid contry, Blank or invalid submitting country",
        LastUpdate: "HCP",
        report: "done",
        VendorName: "Medical Affairs"
      }
    });
  };

  handleCancel = e => {
    this.setState({
      visible: false
    });
  };

  handleCancelPopup = e => {
    this.setState({
      visiblePopup: false
    });
  };

  handleConfirm = e => {
    this.setState({
      visible: false
    });
  };

  render() {
   

    return (
      <React.Fragment>
        {this.state.isLoader ? (
          <Loader />
        ) : (
          <div className="bulk-upd">
            <div className="pop-up">
              <Modal
                className="baseline-modal-showcase"
                visible={this.state.visible}
                onCancel={this.handleCancel}
              >
                <ModalHeader>
                  {" "}
                  <div className="modal-title">
                    {" "}
                    <span className="mod-title">
                      PWC Id {this.state.updatedValue.FileName}
                    </span>
                  </div>{" "}
                </ModalHeader>
                <ModalBody>
                  <div className="remediation-section">
                    <div className="remediation-section-form">
                      <div className="address-lines">
                        <div className="address-one">
                          <label className="address-label">PWC ID</label>
                          <input
                            type="text/number"
                            className="remediation-section-input"
                            name="FileName"
                            onChange={this.upadtedlistdata}
                            value={this.state.updatedValue.FileName}
                          ></input>
                        </div>{" "}
                        <div className="address-two">
                          <label className="address-label">
                            SOURCE SYSTEM NAME
                          </label>
                          <input
                            type="text/number"
                            className="remediation-section-input"
                            name="Status"
                            onChange={this.upadtedlistdata}
                            value={this.state.updatedValue.Status}
                          ></input>
                        </div>
                      </div>
                      <div className="address-lines">
                        <div className="address-one">
                          <label className="address-label">PWC STATUS</label>
                          <input
                            type="text/number"
                            className="remediation-section-input"
                            name="Records"
                            onChange={this.upadtedlistdata}
                            value={this.state.updatedValue.Records}
                          ></input>
                        </div>
                        <div className="address-two">
                          <label className="address-label">
                            PWC REASON CODE
                          </label>
                          <input
                            type="text/number"
                            className="remediation-section-input"
                            name="Errors"
                            onChange={this.upadtedlistdata}
                            value={this.state.updatedValue.Errors}
                          ></input>
                        </div>
                      </div>
                      <div className="address-lines">
                        <div className="address-one">
                          <label className="address-label">RECIPENT TYPE</label>
                          <input
                            type="text/number"
                            className="remediation-section-input"
                            name="LastUpdate"
                            onChange={this.upadtedlistdata}
                            value={this.state.updatedValue.LastUpdate}
                          ></input>
                        </div>
                        <div className="address-two">
                          <label className="address-label">STATUS</label>
                          <input
                            type="text/number"
                            className="remediation-section-input"
                            value={this.state.updatedValue.report}
                            name="report"
                            onChange={this.upadtedlistdata}
                          ></input>
                        </div>
                      </div>
                      <div className="address-lines">
                        <div className="address-one">
                          <label className="address-label">Reportable</label>
                          <input
                            type="text/number"
                            className="remediation-section-input"
                            name="report"
                          ></input>
                        </div>{" "}
                        <div className="address-two">
                          <label className="address-label">
                            CDF Activity Type
                          </label>
                          <div className="remediation-field">
                            <select>
                              <option value="volvo"></option>
                              <option value="saab">filename</option>
                              <option value="opel">Status</option>
                              <option value="audi">Records</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div className="address-lines">
                        <div className="address-one">
                          <label className="address-label">Status</label>
                          <div className="remediation-field">
                            <select>
                              <option value="volvo"></option>
                              <option value="saab">filename</option>
                              <option value="opel">Status</option>
                              <option value="audi">Records</option>
                            </select>
                          </div>
                        </div>{" "}
                        <div className="address-two">
                          <label className="address-label">
                            CDF Expense Type
                          </label>
                          <div className="remediation-field">
                            <select>
                              <option value="volvo"></option>
                              <option value="saab">filename</option>
                              <option value="opel">Status</option>
                              <option value="audi">Records</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div className="address-lines-mdm">
                        <div className="address-one-mdm">
                          <label className="address-label">MDM ID</label>
                          <div className="update-btn">
                            <input
                              type="text/number"
                              className="remediation-section-input-new"
                            ></input>
                          </div>
                        </div>{" "}
                        <div className="address-two">
                          <label className="address-label">ToV Amount</label>
                          <input
                            type="text/number"
                            className="remediation-section-input"
                            name="report"
                          ></input>
                        </div>
                      </div>
                      <div className="address-lines-mdm">
                        <div className="address-one-mdm">
                          <label className="address-label">NPI</label>
                          <div className="update-btn">
                            <div className="reme-checkbox-new">
                              <input
                                type="text/number"
                                className="remediation-section-input-mdm"
                              ></input>
                              <div className="reme-checkbox">
                                <input
                                  type="checkbox"
                                  className="remediation-section-checkbox"
                                  value="No MDM"
                                ></input>
                                <label className="no-mdm">No MDM</label>
                              </div>
                            </div>
                          </div>{" "}
                        </div>
                        <div className="address-two">
                          <label className="address-label">ToV Currency</label>
                          <div className="remediation-field">
                            <select>
                              <option value="volvo"></option>
                              <option value="saab">filename</option>
                              <option value="opel">Status</option>
                              <option value="audi">Records</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div className="address-lines">
                        <div className="address-one">
                          <label className="address-label">
                            MDM Match Confidence
                          </label>
                          <div className="remediation-field">
                            <select>
                              <option value="volvo"></option>
                              <option value="saab">filename</option>
                              <option value="opel">Status</option>
                              <option value="audi">Records</option>
                            </select>
                          </div>
                        </div>{" "}
                        <div className="address-two">
                          <label className="address-label">ToV Date</label>
                          <input
                            type="text/number"
                            className="remediation-section-input"
                          ></input>
                        </div>
                      </div>
                      <div className="address-lines">
                        <div className="address-one">
                          <label className="address-label">First Name</label>
                          <input
                            type="text/number"
                            className="remediation-section-input"
                          ></input>
                        </div>
                        <div className="address-two">
                          <label className="address-label">
                            Institution Recipient Type
                          </label>
                          <div className="remediation-field">
                            <select>
                              <option value="volvo"></option>
                              <option value="saab">filename</option>
                              <option value="opel">Status</option>
                              <option value="audi">Records</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </ModalBody>
                <ModalFooter style={{ justifyContent: "flex-end" }}>
                  <Button kind="transparent" onClick={this.handleConfirm}>
                    {" "}
                  </Button>
                  <button
                    onClick={() =>
                      this.updateTransaction(this.state.updatedValue.FileName)
                    }
                    className="UPDATE"
                  >
                    UPDATE
                  </button>
                </ModalFooter>
              </Modal>
            </div>
            <div className="task-report">
              <div className="task-content">
                <Breadcrum items={items} />
              </div>
              <div className="border-bottom-update"></div>
              <div className="search-bar-bulk">
                <img className="search" src={Search} alt="Search"></img>
                <input
                  type="text"
                  placeholder="Search"
                  onChange={e => this.filterList(e)}
                ></input>
              </div>
              <div className="filter-projects">
                <div className="dropdown-btns">
                  <div className="field-dropdowns">
                    <div className="show-add-popup">
                      <button className="addNew" onClick={this.ShowPopup}>
                        <span className="remediatonlist-plus"> + </span>ADD NEW
                      </button>
                      <div className="edit-popup">
                        <Modal
                          className="baseline-modal-showcase"
                          visible={this.state.visiblePopup}
                          onCancel={this.handleCancelPopup}
                        >
                          <ModalHeader>
                            {" "}
                            <div className="modal-title">
                              {" "}
                              <span
                                className="mod-title"
                                onClick={this.showValue}
                              >
                                PWC Id {this.state.data.FileName}
                              </span>
                            </div>{" "}
                          </ModalHeader>
                          <ModalBody>
                            <div className="remediation-section">
                              <div className="remediation-section-form">
                                <div className="address-lines">
                                  <div className="address-one">
                                    <label className="address-label">
                                      PWC ID
                                    </label>
                                    <input
                                      type="text/number"
                                      className="remediation-section-input"
                                      value={this.state.data.FileName}
                                    ></input>
                                  </div>{" "}
                                  <div className="address-two">
                                    <label className="address-label">
                                      SOURCE SYSTEM NAME
                                    </label>
                                    <input
                                      type="text/number"
                                      className="remediation-section-input"
                                      value={this.state.data.Status}
                                    ></input>
                                  </div>
                                </div>
                                <div className="address-lines">
                                  <div className="address-one">
                                    <label className="address-label">
                                      PWC STATUS
                                    </label>
                                    <input
                                      type="text/number"
                                      className="remediation-section-input"
                                      value={this.state.data.Records}
                                    ></input>
                                  </div>
                                  <div className="address-two">
                                    <label className="address-label">
                                      PWC REASON CODE
                                    </label>
                                    <input
                                      type="text/number"
                                      className="remediation-section-input"
                                      value={this.state.data.Errors}
                                    ></input>
                                  </div>
                                </div>
                                <div className="address-lines">
                                  <div className="address-one">
                                    <label className="address-label">
                                      RECIPENT TYPE
                                    </label>
                                    <input
                                      type="text/number"
                                      className="remediation-section-input"
                                      value={this.state.data.LastUpdate}
                                    ></input>
                                  </div>
                                  <div className="address-two">
                                    <label className="address-label">
                                      STATUS
                                    </label>
                                    <input
                                      type="text/number"
                                      className="remediation-section-input"
                                      value={this.state.data.report}
                                    ></input>
                                  </div>
                                </div>
                                <div className="address-lines">
                                  <div className="address-one">
                                    <label className="address-label">
                                      Reportable
                                    </label>
                                    <input
                                      type="text/number"
                                      className="remediation-section-input"
                                      placeholder="Federal"
                                      name="report"
                                    ></input>
                                  </div>{" "}
                                  <div className="address-two">
                                    <label className="address-label">
                                      CDF Activity Type
                                    </label>
                                    <div className="remediation-field">
                                      <select>
                                        <option value="volvo"></option>
                                        <option value="saab">filename</option>
                                        <option value="opel">Status</option>
                                        <option value="audi">Records</option>
                                      </select>
                                    </div>
                                  </div>
                                </div>
                                <div className="address-lines">
                                  <div className="address-one">
                                    <label className="address-label">
                                      Status
                                    </label>
                                    <div className="remediation-field">
                                      <select>
                                        <option value="volvo"></option>
                                        <option value="saab">filename</option>
                                        <option value="opel">Status</option>
                                        <option value="audi">Records</option>
                                      </select>
                                    </div>
                                  </div>{" "}
                                  <div className="address-two">
                                    <label className="address-label">
                                      CDF Expense Type
                                    </label>
                                    <div className="remediation-field">
                                      <select>
                                        <option value="volvo"></option>
                                        <option value="saab">filename</option>
                                        <option value="opel">Status</option>
                                        <option value="audi">Records</option>
                                      </select>
                                    </div>
                                  </div>
                                </div>
                                <div className="address-lines-mdm">
                                  <div className="address-one-mdm">
                                    <label className="address-label">
                                      MDM ID
                                    </label>
                                    <div className="update-btn">
                                      <input
                                        type="text/number"
                                        className="remediation-section-input-new"
                                      ></input>
                                    </div>
                                  </div>{" "}
                                  <div className="address-two">
                                    <label className="address-label">
                                      ToV Amount
                                    </label>
                                    <input
                                      type="text/number"
                                      className="remediation-section-input"
                                      name="report"
                                    ></input>
                                  </div>
                                </div>
                                <div className="address-lines-mdm">
                                  <div className="address-one-mdm">
                                    <label className="address-label">NPI</label>
                                    <div className="update-btn">
                                      <div className="reme-checkbox-new">
                                        <input
                                          type="text/number"
                                          className="remediation-section-input-mdm"
                                        ></input>
                                        <div className="reme-checkbox">
                                          <input
                                            type="checkbox"
                                            className="remediation-section-checkbox"
                                            value="No MDM"
                                          ></input>
                                          <label className="no-mdm">
                                            No MDM
                                          </label>
                                        </div>
                                      </div>
                                    </div>{" "}
                                  </div>
                                  <div className="address-two">
                                    <label className="address-label">
                                      ToV Currency
                                    </label>
                                    <div className="remediation-field">
                                      <select>
                                        <option value="volvo"></option>
                                        <option value="saab">filename</option>
                                        <option value="opel">Status</option>
                                        <option value="audi">Records</option>
                                      </select>
                                    </div>
                                  </div>
                                </div>
                                <div className="address-lines">
                                  <div className="address-one">
                                    <label className="address-label">
                                      MDM Match Confidence
                                    </label>
                                    <div className="remediation-field">
                                      <select>
                                        <option value="volvo"></option>
                                        <option value="saab">filename</option>
                                        <option value="opel">Status</option>
                                        <option value="audi">Records</option>
                                      </select>
                                    </div>
                                  </div>{" "}
                                  <div className="address-two">
                                    <label className="address-label">
                                      ToV Date
                                    </label>
                                    <input
                                      type="text/number"
                                      className="remediation-section-input"
                                    ></input>
                                  </div>
                                </div>
                                <div className="address-lines">
                                  <div className="address-one">
                                    <label className="address-label">
                                      First Name
                                    </label>
                                    <input
                                      type="text/number"
                                      className="remediation-section-input"
                                    ></input>
                                  </div>
                                  <div className="address-two">
                                    <label className="address-label">
                                      Institution Recipient Type
                                    </label>
                                    <div className="remediation-field">
                                      <select>
                                        <option value="volvo"></option>
                                        <option value="saab">filename</option>
                                        <option value="opel">Status</option>
                                        <option value="audi">Records</option>
                                      </select>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </ModalBody>
                          <ModalFooter style={{ justifyContent: "flex-end" }}>
                            <Button class="trans-cancel">CANCEL</Button>
                            <Button
                              class="trans-save"
                              onClick={this.addNewData}
                            >
                              SAVE
                            </Button>{" "}
                          </ModalFooter>
                        </Modal>
                      </div>
                    </div>
                  </div>
                  <div className="update-table">
                    <React.Fragment>
                      <table>
                        <tr>
                          <th className="update-head">
                            <div className="head-cont-update">
                              <h4 className="filename">PWC ID</h4>
                              <i class="fa fa-sort"></i>
                            </div>
                          </th>
                          <th className="update-head">
                            <div className="head-cont-update">
                              <h4 className="filename">SOURCE SYSTEM NAME </h4>
                              <i className="fa fa-sort"></i>
                            </div>
                          </th>
                          <th className="update-head">
                            <div className="head-cont-update">
                              <h4 className="filename">PWC STATUS </h4>
                              <i className="fa fa-sort"></i>
                            </div>
                          </th>
                          <th className="update-head">
                            <div className="head-cont-update">
                              <h4 className="filename">PWC REASON CODE</h4>
                              <i className="fa fa-sort"></i>
                            </div>
                          </th>
                          <th className="update-head">
                            <div className="head-cont-update">
                              <h4 className="filename">RECIPIENT TYPE</h4>
                              <i className="fa fa-sort"></i>
                            </div>
                          </th>
                          <th className="update-head">
                            <div className="head-cont-update">
                              <h4 className="filename">STATUS</h4>
                              <i className="fa fa-sort"></i>
                            </div>
                          </th>
                          <th></th>
                          <th></th>
                        </tr>
                        {this.state.items.map((item, index) => {
                          return (
                            <tr key={item.FileName}>
                              {item.report === "done" ? (
                                <td
                                  kind="secondary"
                                  onClick={() =>
                                    this.showModal("modal1", item.FileName)
                                  }
                                  size="lg"
                                  className="row-content pwcid"
                                >
                                  {item.FileName}
                                </td>
                              ) : (
                                <td
                                  kind="secondary"
                                  size="lg"
                                  className="row-content pwcid"
                                >
                                  <NavLink
                                    className="remed-nav"
                                    to="/remediationlist"
                                  >
                                    {" "}
                                    {item.FileName}
                                  </NavLink>
                                </td>
                              )}

                              <td className="row-cont-circle">
                                <div className="status-name">{item.Status}</div>
                              </td>
                              <td onClick={this.checking} className="row-cont">
                                {" "}
                                {item.Records}
                              </td>
                              <td className="row-cont">{item.Errors}</td>
                              <td className="row-cont"> {item.LastUpdate}</td>
                              <td className="row-cont">
                                <div
                                  className={this.getCircleClass(item)}
                                ></div>
                              </td>
                              <td className="row-edit">
                                <i
                                  onClick={() =>
                                    this.showModal("modal1", item.FileName)
                                  }
                                  class="fa fa-eye"
                                ></i>
                              </td>
                              <td className="row-edit">
                                {" "}
                                <NavLink to="/remediationlist">
                                <i
                                  
                                  className="fa fa-edit"
                                ></i>
                                </NavLink>
                              </td>
                              <td className="row-delete">
                                <i
                                  onClick={() => this.deleteList(item.FileName)}
                                  className="fa fa-trash"
                                ></i>
                              </td>
                              <td className="radioe"></td>
                            </tr>
                          );
                        })}
                      </table>
                      <PaginationList />
                    </React.Fragment>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
      </React.Fragment>
    );
  }
}
export default Transactionlist;
