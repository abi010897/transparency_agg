import React, { Component } from "react";
import chart from "../../images/chart.png";
import Remediation from "../../images/star.png";
import Transactions from "../../images/transaction.png";
import Report from "../../images/reports.png";
import Data from "../../images/data.png";
import { NavLink } from "react-router-dom";
import "../left-navigation/navigation.scss";

class Navigation extends Component {
  render() {
    return (
      <div className="navigation-bar">
        <div className="sub-navi"></div>
        <div className="dashboard">
          <img className="chart" src={chart} alt=""></img>
          <NavLink to="/" className="decorate">
            <h3 className="dashboard-cont">Dashboard</h3>
          </NavLink>
        </div>
        <div className="nav-content">
          <div className="nav-content-head">
            <img className="img" src={Remediation} alt=""></img>
            <h3 className="head-cont">Data Remediation</h3>
          </div>
          <div className="label-content">
            <NavLink to="/remediation" className="open-task">
              <label className="label-name">Open Tasks (65)</label>
            </NavLink>
            <label className="label-name">Recent Tasks (5)</label>
            <label className="label-name">All Tasks (92)</label>
          </div>
          <div className="nav-content-head">
            <img className="Remediimg" src={Transactions} alt=""></img>
            <h3 className="head-cont">Transactions</h3>
          </div>
          <div className="label-content">
            <label className="label-name">Latest Transactions (3) </label>
            <label className="label-name">All Transactions</label>
          </div>
          <div className="nav-content-head">
            <img className="img" src={Report} alt=""></img>
            <h3 className="head-cont">My Reports</h3>
          </div>
          <div className="label-content">
            <label className="label-name">Analytical Reports</label>
            <NavLink to="/disclosure" className="decorate">
              <label className="label-name">Disclosure Reports</label>
            </NavLink>
          </div>
          <div className="nav-content-head">
            <img className="img" src={Data} alt=""></img>
            <h3 className="head-cont">Data Management</h3>
          </div>
          <div className="label-content">
            <label className="label-name">Vendor Management</label>
            <label className="label-name">Vendor Master</label>
            <label className="label-name">Master Data Lookup</label>
          </div>
        </div>
        <div className="border-bottom"></div>
        <div className="btns">
          <button className="data-btns">
            <span className="plus">+</span>{" "}
            <NavLink to="/fileupload" className="decorate">
              <p className="btn-name">NEW DATA UPLOAD</p>{" "}
            </NavLink>
          </button>

          <button className="data-btns">
            <span className="plus">+</span>{" "}
            <p className="btn-name">NEW REPORT</p>{" "}
          </button>
        </div>
      </div>
    );
  }
}

export default Navigation;
