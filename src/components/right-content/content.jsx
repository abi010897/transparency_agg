import React, { Component } from "react";
import Search from "../../images/Search.png";
import arrow from "../../images/triangle.png";
import AvgImg from "../../images/avg.png";
import BurnRate from "../../images/burn-rate.png";
import finger from "../../images/finger.png";
import Alert from "../../images/alert.png";
import "../right-content/content.scss";

class Content extends Component {
  render() {
    return (
      <div className="content-show">
        <div className="main-content">
          <div className="avail-status">
            <div className="dashboard-show">
              <h3 className="dashboard-name">Dashboard</h3>
              <p className="avail">
                <span className="number">5</span> Escalated Cases Available
              </p>
            </div>
            <div className="search-bar">
              <img className="search" src={Search} alt="Search"></img>
              <input type="text" placeholder="Search"></input>
            </div>
          </div>
          <div className="performance-matrics">
            <h2 className="metrics">Simple Performance Metrics</h2>
            <div className="dropdowns">
              <div className="view-dropdown">
                <p className="view">View:</p>
                <select>
                  <option value="volvo">My metrics</option>
                  <option value="saab">Saab</option>
                  <option value="opel">Opel</option>
                  <option value="audi">Audi</option>
                </select>
              </div>
              <div className="view-dropdown">
                <p className="view sort-by">Sort by:</p>
                <select>
                  <option value="volvo">Week</option>
                  <option value="saab">Saab</option>
                  <option value="opel">Opel</option>
                  <option value="audi">Audi</option>
                </select>
              </div>
            </div>
          </div>
          <div className="cards">
            <div className="label">
              <p className="label-name">LABEL</p>
              <div className="numbers">
                <span className="label-number">12</span>
                <div className="icon-img">
                  <img className="arrow" src={arrow} alt="arrow"></img>
                  <p className="percentage">20%</p>
                </div>
              </div>
              <p className="details">More details</p>
            </div>
            <div className="label">
              <p className="label-name">PENDING REMEDIATION / FOLLOW UP</p>
              <div className="numbers">
                <span className="label-number">8</span>
                <div className="icon-img">
                  <img className="arrow" src={arrow} alt=""></img>
                  <p className="percentage">12%</p>
                </div>
              </div>
              <p className="details">More details</p>
            </div>
            <div className="label">
              <p className="label-name">PENDING APPROVAL</p>
              <div className="numbers">
                <span className="label-number">6</span>
                <div className="icon-img">
                  <img className="arrow" src={arrow} alt=""></img>
                  <p className="percentage">10%</p>
                </div>
              </div>
              <p className="details">More details</p>
            </div>
            <div className="label">
              <p className="label-name">REPORT COMPLETE</p>
              <div className="numbers">
                <span className="label-number">22</span>
                <div className="icon-img">
                  <img className="arrow" src={arrow} alt=""></img>
                  <p className="percentage">14%</p>
                </div>
              </div>
              <p className="details">More details</p>
            </div>
          </div>
          <div className="program-metrics">
            <p className="program-head">Program Metrics</p>
            <div className="bar-charts">
              <img className="burn-rates" src={AvgImg} alt=""></img>
              <img className="burn-rates" src={BurnRate} alt=""></img>
            </div>
          </div>
          <div className="recent-uploads">
            <h2 className="">Recent File Uploads</h2>
            <table>
              <tr>
                <th>CASE ID</th>
                <th>VENDOR/COMPANY</th>
                <th>FILES UPLOAD</th>
                <th>INITIAL UPLOAD FILE</th>
                <th>STATUS</th>
                <th>VENDOR CONTACT</th>
              </tr>
              <tr>
                <td>113-551-Concur-123-818</td>
                <td>Taobao Ltd.</td>
                <td>88</td>
                <td>02/17/2019</td>
                <td>In Progress</td>
                <td>Katie Curry</td>
              </tr>
              <tr>
                <td>113-551-Concur-123-266</td>
                <td>Moonlight Gmbh</td>
                <td>22</td>
                <td>02/20/2019</td>
                <td>Pending Review</td>
                <td>Ernest Simpson</td>
              </tr>
              <tr>
                <td>113-551-Concur-123-371</td>
                <td>E Corp</td>
                <td>6</td>
                <td>02/13/2019</td>
                <td>Report Complete</td>
                <td>Jacob Burgess</td>
              </tr>
              <tr>
                <td>113-551-Concur-123-119</td>
                <td>Taobao Ltd.</td>
                <td>85</td>
                <td>02/17/2019</td>
                <td>In Progress</td>
                <td>Katie Curry</td>
              </tr>
              <tr>
                <td>113-551-Concur-123-503</td>
                <td>Moonlight Gmbh</td>
                <td>14</td>
                <td>02/20/2019</td>
                <td>Pending Review</td>
                <td>Ernest Simpson</td>
              </tr>
              <tr>
                <td>113-551-Concur-123-444</td>
                <td>E Corp</td>
                <td>44</td>
                <td>02/13/2019</td>
                <td>Report Complete</td>
                <td>Jacob Burgess</td>
              </tr>
            </table>
          </div>
          <div className="info-name">
            <div className="base-content">
              <h1 className="info-head">
                New information added in the Knowledge Base
              </h1>
              <p className="update-content">
                Use the updated Knowledge base for tutorials,guidelines and
                other helpful resources in order to stay on top of the latest
                regulatory happenings
              </p>
              <button className="open-btn">OPEN KNOWLEDGE BASE</button>
            </div>
            <div className="base-img">
              <img className="finger" src={finger} alt=""></img>
            </div>
          </div>
          <div className="recent-files">
            <h2 className="name">Recent Files</h2>
            <div className="case-id">
              <div className="sub-case">
                <div className="case-number">
                  <p className="case-head">CASE ID</p>
                  <p className="case-number">113-551-Counur-123-381</p>
                </div>
                <div className="transaction-head">
                  <p className="case-head">TRANSCTIONS</p>
                  <p className="date-number">600</p>
                </div>
                <div className="total-amount">
                  <p className="case-head">TOV AMOUNT</p>
                  <p className="date-number">$14,111.11 (USD)</p>
                </div>
                <div className="date-recipt">
                  <p className="case-head">INITIAL RECEIPT DATE</p>
                  <p className="date-number">02/13/2019</p>
                </div>
                <div className="due-date-recipt">
                  <p className="case-head">DUE DATE</p>
                  <p className="date-number">02/13/2019</p>
                </div>
                <div>
                  <button className="approve">
                    <span className="approve-text">APPROVE</span>
                  </button>
                  <button className="share">
                    <span className="share-text">SHARE</span>
                  </button>
                </div>
              </div>
              <div className="pending-status">
                <button className="pending">PENDING REVIEW</button>
                <div className="alert-box">
                  <img className="alert" src={Alert} alt=""></img>
                  <p className="alert-text">
                    This file has been pending your review,with a due date on{" "}
                    <span className="spl-text">
                      02/15/2019<span className="before">before</span> 11.00 PM
                      CST{" "}
                    </span>{" "}
                  </p>
                </div>
              </div>
            </div>
            <div className="case-id">
              <div className="sub-case">
                <div className="case-number">
                  <p className="case-head">CASE ID</p>
                  <p className="case-number">113-551-Counur-123-381</p>
                </div>
                <div className="transaction-head">
                  <p className="case-head">TRANSCTIONS</p>
                  <p className="date-number">600</p>
                </div>
                <div className="total-amount">
                  <p className="case-head">TOV AMOUNT</p>
                  <p className="date-number">$14,111.11 (USD)</p>
                </div>
                <div className="date-recipt">
                  <p className="case-head">INITIAL RECEIPT DATE</p>
                  <p className="date-number">02/13/2019</p>
                </div>
                <div className="due-date-recipt">
                  <p className="case-head">DUE DATE</p>
                  <p className="date-number">02/13/2019</p>
                </div>
                <div>
                  <button className="share">
                    <span className="share-text">SHARE</span>
                  </button>
                </div>
              </div>
              <div className="pending-status">
                <button className="complete">COMPLETE</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Content;
