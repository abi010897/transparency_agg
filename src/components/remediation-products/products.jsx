import React, { Component } from "react";
import axios from "axios";
import _get from "lodash/get";

class ProductList extends Component {
  constructor() {
    super();
    this.state = {
      sectionProduct: []
    };
  }

  componentDidMount() {
    axios
      .get(`${process.env.REACT_APP_API_URL}/api/steper/transactionListData`)
      .then(res => {
        const data = res.data;
        
        this.setState({ sectionProduct: data.data }, () => {});
      });
  }
  
  render() {
    return (
      <div className="remediation-section">
        <div className="address-lines">
          <div className="address-one">
            <label className="address-label">Product 1</label>
            <input
              type="text/number"
              className="remediation-section-input"
              className={
                _get(this.state.sectionProduct, ["Products", "Products"])
                  ? "remediation-section-input"
                  : "remediation-section-input border-red"
              }
              value={_get(this.state.sectionProduct, ["Products", "Products"])}
              placeholder="Hematolap"
            ></input>
          </div>{" "}
          <div className="address-two">
            <label className="address-label">Product 2</label>
            <input
              type="text"
              className="remediation-section-input"
              className={
                _get(this.state.sectionProduct, ["Products", "Product2"])
                  ? "remediation-section-input"
                  : "remediation-section-input border-red"
              }
              value={_get(this.state.sectionProduct, ["Products", "Product2"])}
              placeholder=""
            ></input>
          </div>
        </div>
        <div className="address-lines">
          <div className="address-one">
            <label className="address-label">Product 3</label>
            <input
              type="text/number"
              className="remediation-section-input"
              className={
                _get(this.state.sectionProduct, ["Products", "Product3"])
                  ? "remediation-section-input"
                  : "remediation-section-input border-red"
              }
              value={_get(this.state.sectionProduct, ["Products", "Product3"])}
              placeholder=""
            ></input>
          </div>{" "}
          <div className="address-two">
            <label className="address-label">Product 4</label>
            <input
              type="text"
              className="remediation-section-input"
              className={
                _get(this.state.sectionProduct, ["Products", "Product4"])
                  ? "remediation-section-input"
                  : "remediation-section-input border-red"
              }
              value={_get(this.state.sectionProduct, ["Products", "Product4"])}
              placeholder=""
            ></input>
          </div>
        </div>
        <div className="address-lines">
          <div className="address-one">
            <label className="address-label">Product 5</label>
            <input
              type="text/number"
              className="remediation-section-input"
              className={
                _get(this.state.sectionProduct, ["Products", "Product 5"])
                  ? "remediation-section-input"
                  : "remediation-section-input border-red"
              }
              value={_get(this.state.sectionProduct, ["Products", "Product 5"])}
              placeholder=""
            ></input>
          </div>{" "}
          <div className="address-two">
            <label className="address-label">Quantity</label>
            <input
              type="text"
              className="remediation-section-input"
              className={
                _get(this.state.sectionProduct, ["Products", "Quantity"])
                  ? "remediation-section-input"
                  : "remediation-section-input border-red"
              }
              value={_get(this.state.sectionProduct, ["Products", "Quantity"])}
              placeholder=""
            ></input>
          </div>
        </div>
        <div className="address-lines">
          <div className="address-one">
            <label className="address-label">Quantity Per Package</label>
            <input
              type="text/number"
              className="remediation-section-input"
              className={
                _get(this.state.sectionProduct, [
                  "Products",
                  "QuantityPerPackage"
                ])
                  ? "remediation-section-input"
                  : "remediation-section-input border-red"
              }
              value={_get(this.state.sectionProduct, [
                "Products",
                "QuantityPerPackage"
              ])}
              placeholder=""
            ></input>
          </div>
        </div>
      </div>
    );
  }
}
export default ProductList;
