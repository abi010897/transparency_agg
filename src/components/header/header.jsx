import React, { Component } from "react";
import comment from "../../images/comment_bubble.png";
import logo from "../../images/logo.png";
import "../header/header.scss";

class Header extends Component {
  constructor() {
    super();
    this.state = {
      showResults: false
    };

    function showUser() {
      this.setState({ showResults: true });
    }
  }
  render() {
    return (
      <div className="header-content">
        <div className="logo">
          <img className="logo" src={logo} alt=""></img>
        </div>
        <div className="side-content">
          <div className="question">?</div>
          <div className="comment">
            <img className="comment" src={comment} alt=""></img>
          </div>
          <div className="login-user">
            <button className="cont">MW</button>
          </div>
          <div className="profile-show" id="Profiles">
            <div className="profile-details">
              <button className="cont">MW</button>
              <div className="user-names">
                <h3 className="mary">Mary Williams</h3>
                <p className="mary-email">mary.s.williams@pwc.com</p>
              </div>
            </div>
            <div className="login-details">
              <label className="profile-names">My Profile</label>
              <label className="profile-names">Data transform settings</label>
              <label className="profile-names">Import settings</label>
              <label className="profile-names">Back to use cases</label>
            </div>
            <div className="logout-btn">
              <button className="logout" onClick={this.showUser}>
                EXIT PROTOTYPE
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Header;
