import React, { Component } from "react";
import axios from "axios";
import _get from "lodash/get";

class StudyInformation extends Component {
  constructor() {
    super();
    this.state = {
      sectionStudy: []
    };
  }

  componentDidMount() {
    axios
      .get(`${process.env.REACT_APP_API_URL}/api/steper/transactionListData`)
      .then(res => {
        const data = res.data;
        
        this.setState({ sectionStudy: data.data }, () => {});
      });
  }

  render() {
    return (
      <div className="remediation-section">
        <div className="address-lines">
          <div className="address-one">
            <label className="address-label">Study Name</label>
            <input
              type="text/number"
              className="remediation-section-input"
              className={
                _get(this.state.sectionStudy, ["StudyInformation", "StudyName"])
                  ? "remediation-section-input"
                  : "remediation-section-input border-red"
              }
              value={_get(this.state.sectionStudy, [
                "StudyInformation",
                "StudyName"
              ])}
              placeholder="Hematolap"
            ></input>
          </div>{" "}
          <div className="address-two">
            <label className="address-label">Research ID</label>
            <input
              type="text"
              className="remediation-section-input"
              className={
                _get(this.state.sectionStudy, [
                  "StudyInformation",
                  "ResearchID"
                ])
                  ? "remediation-section-input"
                  : "remediation-section-input border-red"
              }
              value={_get(this.state.sectionStudy, [
                "StudyInformation",
                "ResearchID"
              ])}
              placeholder=""
            ></input>
          </div>
        </div>
      </div>
    );
  }
}
export default StudyInformation;
