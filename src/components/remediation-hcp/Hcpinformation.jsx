import React, { Component } from "react";
import axios from "axios";
import _get from 'lodash/get';


class HcpInformation extends Component {

  constructor() {
    super()
    this.state = {
      sectionHcp: []
    }

  }


  componentDidMount() {
    axios.get(`${process.env.REACT_APP_API_URL}/api/steper/transactionListData`)
      .then(res => {
        const data = res.data;
       
        this.setState({ sectionHcp: data.data }, () => {

        });
      });
  }

  render() {
    return (
      <div className="remediation-section">
        <div className="remediation-section-form">
          <div className="address-lines">
            <div className="address-one">
              <label className="address-label">Prefix</label>
              <input
                type="text/number"

                className={_get(this.state.sectionHcp, ['HCPInformation', 'Prefix']) ? "remediation-section-input" : "remediation-section-input border-red"}

                value={_get(this.state.sectionHcp, ['HCPInformation', 'Prefix'])}
              ></input>
            </div>{" "}
            <div className="address-two">
              <label className="address-label">Suffix</label>
              <input
                type="text"
                className={_get(this.state.sectionHcp, ['HCPInformation', 'Suffix']) ? "remediation-section-input" : "remediation-section-input border-red"}
                value={_get(this.state.sectionHcp, ['HCPInformation', 'Suffix'])}
              ></input>
            </div>
          </div>
          <div className="address-lines">
            <div className="address-one">
              <label className="address-label">Speciality</label>
              <input
                type="text/number"
                className={_get(this.state.sectionHcp, ['HCPInformation', 'speciality']) ? "remediation-section-input" : "remediation-section-input border-red"}
                value={_get(this.state.sectionHcp, ['HCPInformation', 'speciality'])}
              ></input>
            </div>{" "}
            <div className="address-two">
              <label className="address-label">Speciality Source</label>
              <div className="remediation-field">
                <select>
                  <option value="volvo">Client MDM</option>
                  <option value="saab">filename</option>
                  <option value="opel">Status</option>
                  <option value="audi">Records</option>
                </select>
              </div>
            </div>
          </div>
          <div className="address-lines">
            <div className="address-one">
              <label className="address-label">Professional Designation</label>
              <input
                type="text/number"
                className={_get(this.state.sectionHcp, ['HCPInformation', 'ProfessionalDesignation']) ? "remediation-section-input" : "remediation-section-input border-red"}
                value={_get(this.state.sectionHcp, ['HCPInformation', 'ProfessionalDesignation'])}
              ></input>
            </div>{" "}
            <div className="address-two">
              <label className="address-label">
                Professional Designation Source
              </label>
              <div className="remediation-field">
                <select>
                  <option value="volvo">Client MDM</option>
                  <option value="saab">filename</option>
                  <option value="opel">Status</option>
                  <option value="audi">Records</option>
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default HcpInformation;
