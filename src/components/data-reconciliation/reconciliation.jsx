import React, { Component } from "react";
import Table from "react-bootstrap/Table";
import axios from "axios";
import "./reconciliation.scss";

const initialState = {
  isOpen: false,
  value: "defaultvalue"
};

class Reconciliation extends Component {
  constructor(props) {
    super(props);
    this.state = initialState;
    this.handleClick = this.handleClick.bind(this);
    this.state = {
      initialItems: [
        {
          FileName: "selectAll",
          Status: "select all"
        },
        {
          FileName: "REM-111-1101",
          Status: "REM-111-1101"
        },
        {
          FileName: "MiR Analysis",
          Status: "Pending Approval"
        }
      ],
      allChecked: false,
      checkedCount: 0,
      options: [
        { id: 1, value: "111-124-CN-41", text: "111-124-CN-41" },
        { id: 2, value: "111-124-CN-61", text: "111-124-CN-61" },
        { id: 3, value: "111-124-CN-653", text: "111-124-CN-653" },
        { id: 4, value: "111-124-CN-4", text: "111-124-CN-4" }
      ]
    };
  }
  componentWillMount = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });

    axios
      .get(`${process.env.REACT_APP_API_URL}/duplicateFileList`)
      .then(res => {
        var data = res.data;
        this.setState({ items: data.data });
      });
  };
  toggleModal = () => { };
  toggleModalClose = () => {
    this.setState(initialState);
  };
  handleChange = e => {
    this.setState({
      value: e.target.value
    });
  };
  handleClick(e) {
    let clickedValue = e.target.value;

    if (clickedValue === "selectAll" && this.refs.selectAll.checked) {
      for (let i = 1; i < this.state.options.length; i++) {
        let value = this.state.options[i].value;
        this.refs[value].checked = true;
      }
      this.setState({
        checkedCount: this.state.options.length - 1
      });
    } else if (clickedValue === "selectAll" && !this.refs.selectAll.checked) {
      for (let i = 1; i < this.state.options.length; i++) {
        let value = this.state.options[i].value;
        this.refs[value].checked = false;
      }
      this.setState({
        checkedCount: 0
      });
    }
    if (clickedValue !== "selectAll" && this.refs[clickedValue].checked) {
      this.setState({
        checkedCount: this.state.checkedCount + 1
      });
    } else if (
      clickedValue !== "selectAll" &&
      !this.refs[clickedValue].checked
    ) {
      this.setState({
        checkedCount: this.state.checkedCount - 1
      });
    }
  }

  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {

    return (
      <div className="cont-right">
        <div className="App"></div>
        <div className="desc-div">
          {" "}
          <p className="top-text">Possible Duplicate Records</p>
          <p className="desc">
            Based on previous uploads into the Master Data Record, Transparency
            Hub recocnizes that there are
          </p>
          <p className="desc">
            posiible duplicates in this upload . Confirm and merge the documents
            as needed.
          </p>
        </div>
        <div className="desc-div-below">
          <div className="dot-semi-circle">
            <p className="legent-text">3 Potential cases found</p>
            <span className="legends-black">
              {" "}
              <div className="dot-black "></div> <p>- full match </p>
            </span>
            <span className="legends-partial">
              {" "}
              <div className="dot"></div>
              <p>- partial match</p>{" "}
            </span>
          </div>
        </div>
        <Table striped hover>
          <thead>
            <tr>
              <th className="th-check">
                <input type="checkbox" name="a" />
              </th>
              <th>POTENTIAL MATCH</th>
              <th>TRANSACTION ID</th>
              <th>INVOICE NUMBER</th>
              <th>TRANSACTION DATE</th>
              <th>TOV AMOUNT</th>
              <th>TOV CURRENCY</th>
            </tr>
          </thead>
          <tbody>
            {this.state.options.map(el => (
              <tr>
                <td className="td-check">
                  <input
                    type="checkbox"
                    name={el.value}
                    key={el.value}
                    value={el.value}
                    ref={el.value}
                    name="a"
                    onClick={this.handleClick}
                  />
                </td>
                <td>
                  {" "}
                  <button className="btn-high">HIGH</button>
                </td>
                <td>
                  {" "}
                  <p className="tr-id">{el.text}</p>
                </td>
                <td>
                  <div className="dot-black"></div>
                </td>
                <td>
                  <div className="dot-black"></div>
                </td>
                <td>
                  <div className="dot-black"></div>
                </td>
                <td>
                  <div className="dot"></div>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
    );
  }
}

export default Reconciliation;
