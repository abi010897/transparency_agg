import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Typography from "@material-ui/core/Typography";
import { Button } from "appkit-react";
import { NavLink } from "react-router-dom";
import RemediationSection from "../remediation-section/remediatinsection";
import RemediationAddress from "../remediation-address/remediationAddress";
import HcpInformation from "../remediation-hcp/Hcpinformation";
import License from "../remediaton-license/licenses.jsx";
import "../stepper-horiz/stepper.scss";
import ProductList from "../remediation-products/products";
import StudyInformation from "../remediaton-studyInformation/information";
import axios from 'axios';

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%"
  },
  backButton: {
    marginRight: theme.spacing(1)
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1)
  }
}));

function getSteps() {
  return [
    "Remediation Section",
    "Address",
    "HCP Information",
    "State Licenses",
    "Products",
    "Study Information"
  ];
}
function getStepContent(stepIndex) {
  switch (stepIndex) {
    case 0:
      return <RemediationSection />;
    case 1:
      return <RemediationAddress />;
    case 2:
      return <HcpInformation />;
    case 3:
      return <License />;
    case 4:
      return <ProductList />;
    case 5:
      return <StudyInformation />;
    default:
      return "Study Information";
  }
}

export default function CustomizedSteppers() {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const steps = getSteps();

  const handleNext = () => {
    let updatedObject = JSON.parse(localStorage.getItem('third'));

    if ( updatedObject && Object.keys(updatedObject).length > 0 && updatedObject.constructor === Object) {
      axios.patch(`${process.env.REACT_APP_API_URL}/api/remediationStepper/update`, updatedObject)
        .then((response) => {
          

          localStorage.removeItem('third')
        });
    }
    
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  };



  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };

  return (
    <div>
      <div className="alignment-top">
        <NavLink to="/" className="decorate">
          <Button className="btn-Uppercase cancelbtn">Cancel</Button>
        </NavLink>
        <span className="pipe"></span>
        <Button
          disabled={activeStep === 0}
          onClick={handleBack}
          className={classes.backButton}
          className={activeStep === 0 ? "active-backbtn" : "disabled-backbtn"}
        >
          BACK
        </Button>

        <Button
          variant="contained"
          color="primary"
          onClick={handleNext}
          className="nxtbtn"
        >
          {activeStep === steps.length - 1 ? (
            <NavLink className="remed-nav finish-btn" to="/transactionlist">
              FINISH
            </NavLink>
          ) : (
              "NEXT"
            )}
        </Button>
      </div>
      <div className={classes.root}>
        <Stepper activeStep={activeStep} alternativeLabel>
          {steps.map(label => (
            <Step key={label}>
              <StepLabel>{label}</StepLabel>
            </Step>
          ))}
        </Stepper>
        <div>
          {activeStep === steps.length ? (
            <div>
              <Typography
                component={"div"}
                className={classes.instructions}
              ></Typography>
              <div className="reme-save">
                <Button class="reme-save">SAVE</Button>
              </div>
            </div>
          ) : (
              <div>
                <Typography component={"div"} className={classes.instructions}>
                  {getStepContent(activeStep)}
                </Typography>
                <div className="alignment-right-reme">
                  <div className="reme-btns">
                    <div className="reme-save">
                      <Button class="reme-save">SAVE</Button>
                    </div>
                    <div className="reme-exports-btn">
                      <Button class="reme-export">EXPORT ALL</Button>
                      <Button class="reme-export">TRANS.EXPORT</Button>
                      <Button class="reme-export">STATUS NOTES</Button>
                      <Button class="reme-export">
                        BREAKOUT TRANSACTIONS(0)
                    </Button>
                      <div className="reme-save">
                        <NavLink className="remed-nav" to="/transactionlist">
                          <Button class="reme-save">SUBMIT</Button>
                        </NavLink>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            )}
        </div>
      </div>
    </div>
  );
}
