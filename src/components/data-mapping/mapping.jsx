import React, { Component } from "react";
import {
  Accordions,
  AccordionItem,
  AccordionItemHeader,
  AccordionItemBody
} from "appkit-react";
import "../data-mapping/mapping.scss";

class Mapping extends Component {
  constructor() {
    super();
    this.state = {
      fileNames: [],
      expanded: 1
    };
  }

  componentWillMount() {
    var uploadFileNames = localStorage.getItem("fileNames");
    var parseData = JSON.parse(uploadFileNames);
    if (parseData !== null) {
      this.setState({ expanded: parseData[0].filename });
    }

    return this.setState({ fileNames: parseData });
  }

  render() {
    return (
      <div className="dynamic_forms">
        <div className="data_fields">
          <h3>Data Fields Require Mapping</h3>
          <span className="description">
            Based on the upload configuration and files uploaded, there ares
            multiple fields that need to be organized before remediation
            <br />
            We've selected a coupleof suggestions for you to get started, feel
            free to change them as needed
          </span>
        </div>
        <div>
          {this.state.fileNames === null ? (
            <div>
              <h4>There are no data mapping file uploades</h4>
            </div>
          ) : (
            <Accordions
              className="accordion-example"
              activeItem={this.state.expanded}
            >
              {this.state.fileNames.map((el, i) => (
                <AccordionItem itemId={el.filename}>
                  <AccordionItemHeader
                    className="pad-top"
                    title={el.filename}
                  />
                  <AccordionItemBody>
                    <div className="table">
                      <table>
                        <thead>
                          <tr>
                            <th className="font-size">Source Field Name</th>
                            <th className="font-size">Suggested Field Name</th>
                            <th className="font-size">Required</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr className="bg-color">
                            <td>Recipient</td>
                            <td>
                              <select
                                name="search_categories"
                                id="search_categories"
                              >
                                <option value="1">Recipient Type</option>
                                <option value="2">Recipient Name1</option>
                                <option value="3">Recipient Name2</option>
                                <option value="4">Recipient Name3</option>
                              </select>
                            </td>
                            <td>
                              <input type="checkbox" name="a" />
                            </td>
                          </tr>
                          <tr className="bg-color">
                            <td>Activity Type</td>
                            <td>
                              <select
                                name="search_categories"
                                id="search_categories"
                              >
                                <option value="1">CDF Activity Type</option>
                                <option value="2">Activity Type1</option>
                                <option value="3">Activity Type2</option>
                                <option value="4">Activity Type3</option>
                              </select>
                            </td>
                            <td>
                              <input type="checkbox" name="a" />
                            </td>
                          </tr>
                          <tr className="bg-color">
                            <td>Currency Type</td>
                            <td>
                              <select
                                name="search_categories"
                                id="search_categories"
                              >
                                <option value="1">ToV Currency</option>
                                <option value="2">Currency1</option>
                                <option value="3">Currency2</option>
                                <option value="4">Currency3</option>
                              </select>
                            </td>
                            <td>
                              <input type="checkbox" name="a" />
                            </td>
                          </tr>
                          <tr className="bg-color">
                            <td>Location</td>
                            <td>
                              <select
                                name="search_categories"
                                id="search_categories"
                              >
                                <option value="1">Transaction Location</option>
                                <option value="2">Location1</option>
                                <option value="3">Location2</option>
                                <option value="4">Location3</option>
                              </select>
                            </td>
                            <td>
                              <input type="checkbox" name="a" />
                            </td>
                          </tr>
                          <tr className="bg-color">
                            <td>Name 1</td>
                            <td>
                              <select
                                name="search_categories"
                                id="search_categories"
                              >
                                <option value="1">First Name</option>
                                <option value="2">First Name1</option>
                                <option value="3">First Name2</option>
                                <option value="4">First Name3</option>
                              </select>
                            </td>
                            <td>
                              <input type="checkbox" name="a" />
                            </td>
                          </tr>
                          <tr className="bg-color">
                            <td>Name 2</td>
                            <td>
                              <select
                                name="search_categories"
                                id="search_categories"
                              >
                                <option value="1">Last Name</option>
                                <option value="2">Last Name1</option>
                                <option value="3">Last Name2</option>
                                <option value="4">Last Name3</option>
                              </select>
                            </td>
                            <td>
                              <input type="checkbox" name="a" />
                            </td>
                          </tr>
                          <tr className="bg-color">
                            <td>Research Rule</td>
                            <td>
                              <select
                                name="search_categories"
                                id="search_categories"
                              >
                                <option value="1">Research Type</option>
                                <option value="2">Research Rate</option>
                              </select>
                            </td>
                            <td>
                              <input type="checkbox" name="a" />
                            </td>
                          </tr>
                          <tr className="bg-color">
                            <td>Email</td>
                            <td>
                              <select
                                name="search_categories"
                                id="search_categories"
                              >
                                <option value="1">sample@gmail.com</option>
                                <option value="2">sample1@gmail.com</option>
                              </select>
                            </td>
                            <td>
                              <input type="checkbox" name="a" />
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </AccordionItemBody>
                </AccordionItem>
              ))}
            </Accordions>
          )}
        </div>
      </div>
    );
  }
}
export default Mapping;
