import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Typography from "@material-ui/core/Typography";
import { Button } from "appkit-react";
import { NavLink } from "react-router-dom";
import UploadFile from "../../common/upload-file/uploadfile";
import Mapping from "../data-mapping/mapping";
import Reconciliation from "../data-reconciliation/reconciliation";
import Confirmation from "../data-confirmation/confirmation";
import "../stepper-horiz/stepper.scss";
import Popup from "../popup/popup";
const useStyles = makeStyles(theme => ({
  root: {
    width: "100%"
  },
  backButton: {
    marginRight: theme.spacing(1)
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1)
  }
}));
function getSteps() {
  return ["Data Upload", "Mapping", "Data Reconciliation", "Confirmation"];
}
function getStepContent(stepIndex) {
  switch (stepIndex) {
    case 0:
      return <UploadFile />;
    case 1:
      return <Mapping />;
    case 2:
      return <Reconciliation />;
    case 3:
      return <Confirmation />;
    default:
      return "Unknown stepIndex";
  }
}
export default function CustomizedSteppers() {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const steps = getSteps();
  const handleNext = activeIndex => {
   
    if (activeIndex === 1) {
    } else {
      setActiveStep(prevActiveStep => prevActiveStep + 1);
    }
  };

  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };
  const handleReset = () => {
    setActiveStep(0);
  };
  return (
    <div>
      <div className="alignment-top">
        <NavLink to="/" className="decorate">
          <Button className="btn-Uppercase cancelbtn">Cancel</Button>
        </NavLink>
        <span className="pipe"></span>
        <Button
          disabled={activeStep === 0}
          onClick={handleBack}
          className={classes.backButton}
          className={activeStep === 0 ? "active-backbtn" : "disabled-backbtn"}
        >
          BACK
        </Button>
        <Button
          variant="contained"
          color="primary"
          onClick={handleNext}
          className="nxtbtn"
        >
          {activeStep === steps.length - 1 ? "FINISH" : "NEXT"}
        </Button>
      </div>
      <div className={classes.root}>
        <Stepper activeStep={activeStep} alternativeLabel>
          {steps.map(label => (
            <Step key={label}>
              <StepLabel>{label}</StepLabel>
            </Step>
          ))}
        </Stepper>
        <div>
          {activeStep === steps.length ? (
            <div>
              <Typography component={"div"} className={classes.instructions}>
                <Popup />
              </Typography>
              <Button onClick={handleReset} className="nxtbtn">
                Reset
              </Button>
            </div>
          ) : (
            <div>
              <Typography component={"div"} className={classes.instructions}>
                {getStepContent(activeStep)}
              </Typography>
              <div className="alignment-right">
                <NavLink to="/" className="decorate">
                  <Button className="btn-Uppercase cancelbtn">Cancel</Button>
                </NavLink>
                <span className="pipe"></span>
                <Button
                  disabled={activeStep === 0}
                  onClick={handleBack}
                  className={classes.backButton}
                  className={
                    activeStep === 0 ? "active-backbtn" : "disabled-backbtn"
                  }
                >
                  BACK
                </Button>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={handleNext}
                  className="nxtbtn"
                >
                  {activeStep === steps.length - 1 ? "FINISH" : "NEXT"}
                </Button>
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
