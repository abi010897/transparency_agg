import React, { Component } from "react";
import tickImag from "../../images/tick.png";
import { NavLink } from "react-router-dom";

class Modelfollow extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <div className="popup">
        <div className="popup_inner">
          <h4 className="upload">{this.props.text}</h4>
          <div className="tick_imag">
            <img src={tickImag} alt="" />
          </div>
          <h4 className="work-done">
            Frank will receive a notification and once they complete the
            follow-up, you'll get a notification in your global navigation bar.
          </h4>
          <div className="popup-btns followup-close">
            <NavLink to="/remediationlist" className="decorate">
              <button
                className="a-btn btn-Uppercase cancelbtn a-btn-primary a-btn-sm"
                type="button"
              >
                CLOSE
              </button>
            </NavLink>
          </div>
        </div>
      </div>
    );
  }
}
export default Modelfollow;
