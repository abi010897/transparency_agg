import React, { Component } from "react";
import Modelfollow from "./modelfollow";
import "./popup.scss";

class Followpopup extends Component {
  constructor(props) {
    super(props);
    this.state = { showFollowpopup: false };
  }
  componentWillMount() {
    this.togglePopup();
  }
  togglePopup() {
    this.setState({
      showFollowpopup: !this.state.showFollowpopup
    });
    localStorage.removeItem("fileNames");
  }

  render() {
    return (
      <div>
        {this.state.showFollowpopup ? (
          <Modelfollow
            text="Follow up request has been sent"
            closePopup={this.togglePopup.bind(this)}
          />
        ) : null}
      </div>
    );
  }
}
export default Followpopup;
