import React, { Component } from "react";
import Modelpopup from "./modelpopup";
import "./popup.scss";

class Popup extends Component {
  constructor(props) {
    super(props);
    this.state = { showPopup: false };
  }
  componentWillMount() {
    this.togglePopup();
  }
  togglePopup() {
    this.setState({
      showPopup: !this.state.showPopup
    });
    localStorage.removeItem("fileNames");
  }

  render() {
    return (
      <div>
        {this.state.showPopup ? (
          <Modelpopup
            text="Upload Complete"
            closePopup={this.togglePopup.bind(this)}
          />
        ) : null}
      </div>
    );
  }
}
export default Popup;
