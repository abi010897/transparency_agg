import React, { Component } from "react";
import tickImag from "../../images/tick.png";
import { NavLink } from "react-router-dom";

class Modelpopup extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <div className="popup">
        <div className="popup_inner">
          <h4 className="upload">{this.props.text}</h4>
          <div className="tick_imag">
            <img src={tickImag} alt="" />
          </div>
          <h4 className="work-done">
            Nice work! All the transactions are ready for remediation
          </h4>
          <div className="popup-btns">
            <NavLink to="/remediation" className="decorate">
              <button className="remediation">BEGIN REMEDIATION</button>
            </NavLink>
            <NavLink to="/" className="decorate">
              <button onClick={this.props.closePopup} className="return-btn">
                {" "}
                Return to dashboard
              </button>
            </NavLink>
          </div>
        </div>
      </div>
    );
  }
}
export default Modelpopup;
