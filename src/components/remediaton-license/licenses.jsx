import React, { Component } from "react";
import axios from "axios";
import _get from "lodash/get";

class License extends Component {
  constructor() {
    super();
    this.state = {
      sectionLiscence: []
    };
  }

  componentDidMount() {
    axios
      .get(`${process.env.REACT_APP_API_URL}/api/steper/transactionListData`)
      .then(res => {
        const data = res.data;
        
        this.setState({ sectionLiscence: data.data }, () => {});
      });
  }
  
  render() {
    return (
      <div className="remediation-section">
        <div className="remediation-section-form">
          <div className="address-lines">
            <div className="address-one">
              <label className="address-label">MA SLN</label>
              <input
                type="text/number"
                className={
                  _get(this.state.sectionLiscence, ["StateLisences", "MASLN"])
                    ? "remediation-section-input"
                    : "remediation-section-input border-red"
                }
                value={_get(this.state.sectionLiscence, [
                  "StateLisences",
                  "MASLN"
                ])}
                placeholder=""
              ></input>
            </div>{" "}
            <div className="address-two">
              <label className="address-label">MA SLN Source</label>
              <input
                type="text"
                className={
                  _get(this.state.sectionLiscence, [
                    "StateLisences",
                    "MASLNSource"
                  ])
                    ? "remediation-section-input"
                    : "remediation-section-input border-red"
                }
                value={_get(this.state.sectionLiscence, [
                  "StateLisences",
                  "MASLNSource"
                ])}
                placeholder=""
              ></input>
            </div>
          </div>
          <div className="address-lines">
            <div className="address-one">
              <label className="address-label">VT SLN</label>
              <input
                type="text/number"
                className={
                  _get(this.state.sectionLiscence, ["StateLisences", "VTSLN"])
                    ? "remediation-section-input"
                    : "remediation-section-input border-red"
                }
                value={_get(this.state.sectionLiscence, [
                  "StateLisences",
                  "VTSLN"
                ])}
                placeholder=""
              ></input>
            </div>{" "}
            <div className="address-two">
              <label className="address-label">VT SLN Source</label>
              <div className="remediation-field">
                <select>
                  <option value="volvo">filename</option>
                  <option value="saab">filename</option>
                  <option value="opel">Status</option>
                  <option value="audi">Records</option>
                </select>
              </div>
            </div>
          </div>
          <div className="address-lines">
            <div className="address-one">
              <label className="address-label">DC SLN</label>
              <input
                type="text/number"
                className={
                  _get(this.state.sectionLiscence, ["StateLisences", "DCSLN"])
                    ? "remediation-section-input"
                    : "remediation-section-input border-red"
                }
                value={_get(this.state.sectionLiscence, [
                  "StateLisences",
                  "DCSLN"
                ])}
                placeholder="MD"
              ></input>
            </div>{" "}
            <div className="address-two">
              <label className="address-label">DC SLN Source</label>
              <div className="remediation-field">
                <select>
                  <option value="volvo">filename</option>
                  <option value="saab">filename</option>
                  <option value="opel">Status</option>
                  <option value="audi">Records</option>
                </select>
              </div>
            </div>
          </div>
          <div className="address-lines">
            <div className="address-one">
              <label className="address-label">CA SLN</label>
              <input
                type="text/number"
                className={
                  _get(this.state.sectionLiscence, ["StateLisences", "CASLN"])
                    ? "remediation-section-input"
                    : "remediation-section-input border-red"
                }
                value={_get(this.state.sectionLiscence, [
                  "StateLisences",
                  "CASLN"
                ])}
                placeholder=""
              ></input>
            </div>{" "}
            <div className="address-two">
              <label className="address-label">CA SLN Source</label>
              <div className="remediation-field">
                <select>
                  <option value="volvo">filename</option>
                  <option value="saab">filename</option>
                  <option value="opel">Status</option>
                  <option value="audi">Records</option>
                </select>
              </div>
            </div>
          </div>
          <div className="address-lines">
            <div className="address-one">
              <label className="address-label">MN SLN</label>
              <input
                type="text/number"
                className={
                  _get(this.state.sectionLiscence, ["StateLisences", "MNSLN"])
                    ? "remediation-section-input"
                    : "remediation-section-input border-red"
                }
                value={_get(this.state.sectionLiscence, [
                  "StateLisences",
                  "MNSLN"
                ])}
                placeholder=""
              ></input>
            </div>{" "}
            <div className="address-two">
              <label className="address-label">MN SLN Source</label>
              <div className="remediation-field">
                <select>
                  <option value="volvo">filename</option>
                  <option value="saab">filename</option>
                  <option value="opel">Status</option>
                  <option value="audi">Records</option>
                </select>
              </div>
            </div>
          </div>
          <div className="address-lines">
            <div className="address-one">
              <label className="address-label">CT SLN</label>
              <input
                type="text/number"
                className={
                  _get(this.state.sectionLiscence, ["StateLisences", "CTSLN"])
                    ? "remediation-section-input"
                    : "remediation-section-input border-red"
                }
                value={_get(this.state.sectionLiscence, [
                  "StateLisences",
                  "CTSLN"
                ])}
                placeholder=""
              ></input>
            </div>{" "}
            <div className="address-two">
              <label className="address-label">CT SLN Source</label>
              <div className="remediation-field">
                <select>
                  <option value="volvo">filename</option>
                  <option value="saab">filename</option>
                  <option value="opel">Status</option>
                  <option value="audi">Records</option>
                </select>
              </div>
            </div>
          </div>
          <div className="address-lines">
            <div className="address-one">
              <label className="address-label">Other SLN</label>
              <input
                type="text/number"
                className={
                  _get(this.state.sectionLiscence, [
                    "StateLisences",
                    "OtherSLN"
                  ])
                    ? "remediation-section-input "
                    : "remediation-section-input border-red"
                }
                value={_get(this.state.sectionLiscence, [
                  "StateLisences",
                  "OtherSLN"
                ])}
              ></input>
            </div>{" "}
            <div className="address-two">
              <label className="address-label">Other SLN Source</label>
              <div className="remediation-field">
                <select>
                  <option value="volvo">Client MDM</option>
                  <option value="saab">filename</option>
                  <option value="opel">Status</option>
                  <option value="audi">Records</option>
                </select>
              </div>
            </div>
          </div>
          <div className="address-lines">
            <div className="address-one">
              <label className="address-label">Other SLN</label>
              <input
                type="text/number"
                className={
                  _get(this.state.sectionLiscence, [
                    "StateLisences",
                    "OtherSLN"
                  ])
                    ? "remediation-section-input "
                    : "remediation-section-input border-red"
                }
                value={_get(this.state.sectionLiscence, [
                  "StateLisences",
                  "OtherSLN"
                ])}
              ></input>
            </div>{" "}
            <div className="address-two">
              <label className="address-label">Other SLN Source</label>
              <div className="remediation-field">
                <select>
                  <option value="volvo">Client MDM</option>
                  <option value="saab">filename</option>
                  <option value="opel">Status</option>
                  <option value="audi">Records</option>
                </select>
              </div>
            </div>
          </div>
          <div className="address-lines">
            <div className="address-one">
              <label className="address-label">Other SLN</label>
              <input
                type="text/number"
                className={
                  _get(this.state.sectionLiscence, [
                    "StateLisences",
                    "OtherSLN"
                  ])
                    ? "remediation-section-input "
                    : "remediation-section-input border-red "
                }
                value={_get(this.state.sectionLiscence, [
                  "StateLisences",
                  "OtherSLN"
                ])}
              ></input>
            </div>{" "}
            <div className="address-two">
              <label className="address-label">Other SLN Source</label>
              <div className="remediation-field">
                <select>
                  <option value="volvo">Client MDM</option>
                  <option value="saab">filename</option>
                  <option value="opel">Status</option>
                  <option value="audi">Records</option>
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default License;
