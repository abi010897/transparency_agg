import React, { Component } from "react";
import Content from "../right-content/content";
import Loader from "../../common/loader/loader";
import { setTimeout } from "timers";
import "../dashboard/dashboard.scss";

class Dashboard extends Component {
  componentWillMount() {
    setTimeout(() => {
      this.setState({ isLoader: false });
    }, 1000);
  }

  state = {
    isLoader: true
  };
  render() {
    return (
      <React.Fragment>
        {this.state.isLoader ? (
          <Loader />
        ) : (
          <div className="cont-right">
            <Content />
          </div>
        )}
      </React.Fragment>
    );
  }
}

export default Dashboard;
