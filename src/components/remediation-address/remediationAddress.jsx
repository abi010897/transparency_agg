import React, { Component } from "react";
import "../remediation-address/remediationAddress.scss";
import axios from "axios";
import _get from "lodash/get";

class RemediationAddress extends Component {
  constructor() {
    super();
    this.state = {
      sectionAddress: []
    };
  }

  componentDidMount() {
    axios
      .get(`${process.env.REACT_APP_API_URL}/api/steper/transactionListData`)
      .then(res => {
        const data = res.data;
        this.setState({ sectionAddress: data.data }, () => {
        });
      });
  }

  inputChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;

    this.setState(prevState => ({
      sectionAddress: {
        ...prevState.sectionAddress,
        address: {
          ...prevState.sectionAddress.address,
          [name]: value,
        }
      },
      updateObject: { ...prevState.updateObject, [name]: value }
    })



    );



    localStorage.setItem("third", JSON.stringify(this.state.updateObject));
  }


  render() {
   
    return (
      <div className="remediation-section">
        <div className="remediation-section-form">
          <div className="address-lines">
            <div className="address-one">
              <label className="address-label">Address Line 1</label>
              <input
                type="text/number"
                name="AddressLine1"
                onChange={this.inputChange}
                className="remediation-section-input"
                value={_get(this.state.sectionAddress, [
                  "address",
                  "AddressLine1"
                ])}
              ></input>
            </div>{" "}
            <div className="address-two">
              <label className="address-label">Address Line 2</label>
              <input
                type="text"
                className="remediation-section-input"
                value={_get(this.state.sectionAddress, [
                  "address",
                  "AddressLine2"
                ])}
              ></input>
            </div>
          </div>
          <div className="address-lines">
            <div className="address-one">
              <label className="address-label">Address Line 3</label>
              <input
                type="text/number"
                className={
                  _get(this.state.sectionAddress, ["address", "AddressLine3"])
                    ? "remediation-section-input"
                    : "remediation-section-input border-red"
                }
                value={_get(this.state.sectionAddress, [
                  "address",
                  "AddressLine3"
                ])}
              ></input>
            </div>{" "}
            <div className="address-two">
              <label className="address-label">City</label>
              <input
                type="text"
                name="city"
                onChange={this.inputChange}
                className="remediation-section-input"
                value={_get(this.state.sectionAddress, ["address", "city"])}
              ></input>
            </div>
          </div>
          <div className="address-lines">
            <div className="address-one">
              <label className="address-label">State</label>
              <input
                type="text/number"
                className="remediation-section-input"
                value={_get(this.state.sectionAddress, ["address", "State"])}
              ></input>
            </div>{" "}
            <div className="address-two">
              <label className="address-label">Postal Code</label>
              <input
                type="text/number"
                className="remediation-section-input"
                value={_get(this.state.sectionAddress, [
                  "address",
                  "PostalCode"
                ])}
              ></input>
            </div>
          </div>
          <div className="address-lines">
            <div className="address-one">
              <label className="address-label">Country</label>
              <div className="remediation-field">
                <select>
                  <option value="volvo">US</option>
                  <option value="saab">filename</option>
                  <option value="opel">Status</option>
                  <option value="audi">Records</option>
                </select>
              </div>
            </div>{" "}
            <div className="address-two">
              <label className="address-label">Address Source</label>
              <div className="remediation-field">
                <select>
                  <option value="volvo">Client MDM</option>
                  <option value="saab">filename</option>
                  <option value="opel">Status</option>
                  <option value="audi">Records</option>
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default RemediationAddress;
