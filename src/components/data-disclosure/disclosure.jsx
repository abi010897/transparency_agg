import React, { Component } from "react";
import axios from "axios";
import Search from "../../images/Search.png";
import "../data-disclosure/disclosure.scss";




class Disclosure extends Component {



  constructor() {
    super();
    this.state = {

      countryUrl: "/reports/Disclosure_Report_USA.csv",

      options: [
        {
          name: 'USA',
          value: 'USA',
        },
        {
          name: 'Colombia',
          value: 'Colombia',
        },
        {
          name: 'Germany',
          value: 'Germany',
        },
        {
          name: 'Belgium',
          value: 'Belgium',
        },
      ],


    };
  }

  handleChange = (event) => {
    let fileUrl;
    switch (event.target.value) {
      case 'Colombia': fileUrl = '/reports/Disclosure_Report_Colombia.csv';
        break;
      case 'Belgium': fileUrl = '/reports/BE_Belgium_Disclosure_Report.xlsx';
        break;
      case 'Germany': fileUrl = '/reports/DE_Germany_Disclosure_Report.xlsx';
        break;
      default: fileUrl = 'reports/Disclosure_Report_USA.csv';
    }
    this.setState({
      value: event.target.value,
      countryUrl: fileUrl,
    });

  };

  downloadExcelReport = () => {
    axios
      .get(
        `${process.env.REACT_APP_API_URL}/api/disclosure-report/download?type=report1`
      )
      .then(res => {
       
      });
  };







  render() {
   

    const { options, value } = this.state;

    return (
      <div className="disclosure-wrapper">
        <div className="disclosure-container">
          <div className="disclosure-head">
            <div className="crum-card">
              <div className="report-content">
                <p className="task-name">
                  <i class="fa fa-home"></i>
                  &nbsp;&nbsp;<span className="bread-slash">/</span>&nbsp;&nbsp;
                  My Reports &nbsp;&nbsp;<span className="bread-slash">/</span>
                  &nbsp;&nbsp; Disclosure Reports{" "}
                </p>
              </div>
            </div>
            <div className="search-bar">
              <img className="search" src={Search} alt="Search"></img>
              <input type="text" placeholder="Search"></input>
            </div>
          </div>
          <div className="report-page">
            <h2 className="disclosure-name">Generate Disclosure Report</h2>
            <div className="disclosure-table">
              <table className="disclosure--table-head">
                <thead className="disclosure-thead">
                  <tr>
                    <th>SOURCE FIELD NAME</th>
                    <th>SUGGESTED FIELD NAME</th>
                  </tr>
                </thead>
                <tbody className="disclosure-body">
                  <tr className="disclosure-row">
                    <td>Country</td>
                    <td>
                      <div className="remediation-field">
                        <select onChange={this.handleChange} value={value}>

                          {this.state.options.map(item => (
                            <option key={item.value}
                              value={item.value}>
                              {item.name}
                            </option>
                          ))}

                        </select>

                      </div>
                    </td>
                  </tr>
                  <tr className="disclosure-row">
                    <td>Report</td>
                    <td>
                      <div className="remediation-field">
                        <select>
                          <option value="volvo">Federal Report</option>
                          <option value="saab">filename</option>
                          <option value="opel">Status</option>
                          <option value="audi">Records</option>
                        </select>
                      </div>
                    </td>
                  </tr>
                  <tr className="disclosure-row">
                    <td>Year</td>
                    <td>
                      <div className="remediation-field">
                        <select>
                          <option value="volvo">2019</option>
                          <option value="saab">2018</option>
                          <option value="opel">2017</option>
                          <option value="audi">2016</option>
                        </select>
                      </div>
                    </td>
                  </tr>
                  <tr className="disclosure-row">
                    <td>Type</td>
                    <td>
                      <div className="remediation-field">
                        <select>
                          <option value="volvo">Draft</option>
                          <option value="saab">PPT</option>
                          <option value="opel">Status</option>
                          <option value="audi">Records</option>
                        </select>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div className="disclosure-btns">
            <button className="disclosure-cancel">CANCEL</button>
            <button
              onClick={this.downloadExcelReport}
              className="disclosure-export"
            >
              <a
                href={`${process.env.REACT_APP_API_URL}${this.state.countryUrl}`}
                className="download"
                target="_blank"
                download
              >
                EXPORT
              </a>
            </button>
          </div>
        </div>
      </div>
    );
  }
}
export default Disclosure;
