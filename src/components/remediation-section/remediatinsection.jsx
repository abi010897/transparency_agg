import React, { Component } from "react";
import "../remediation-section/remediationsection.scss";
import Search from "../../images/Search.png";
import Frank from "../../images/Frank-Kenney.jpg";
import Kay from "../../images/Kay-Tot.jpg";
import axios from "axios";
import _get from "lodash/get";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from "appkit-react";
import { NavLink } from "react-router-dom";

class RemediationSection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visiblePopup: false,
      sectionData: {},
      radioValue: "radio1",
      setFollowup: "",
      value: 'select',
      newdate: '',
      newCurrency: '',
      updateObject: {},
    };
  }
  componentDidMount() {
    axios
      .get(`${process.env.REACT_APP_API_URL}/api/steper/transactionListData`)
      .then(res => {
        const data = res.data;
        this.setState({ sectionData: data.data }, () => {
        });
      });
  }
  showFollow = () => {
    this.setState({
      visiblePopup: true,

    });
  };

  setFollowupNote(note) {

    this.setState({ setFollowup: note.target.value });
  }

  handleCancelPopup = e => {
    this.setState({
      visiblePopup: false
    });
  };

  handleRadioChange = e => {
    const name = e.target.name;
    this.setState({ radioValue: name });
  };


  tovCurrency = (event) => {
    this.setState({

      newCurrency: event.target.value

    });
    localStorage.setItem("sec", JSON.stringify(this.state.newCurrency));

  }

  changeDate = (e) => {
    
    this.setState({

      newdate: e.target.value

    })
    localStorage.setItem("first", JSON.stringify(this.state.newdate));
  }



  inputChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;

    this.setState(prevState => ({
      sectionData: {
        ...prevState.sectionData,
        remediationSection: {
          ...prevState.sectionData.remediationSection,
          [name]: value,
        }
      },
      updateObject: { ...prevState.updateObject, [name]: value }
    })



    );



    localStorage.setItem("third", JSON.stringify(this.state.updateObject));
  }
  render() {

   
    return (
      <div className="remediation-section">
        <div className="remediation-section-form">
          <div className="section-border">
            <div class="tab"></div>
            <div className="address-lines">
              <div className="address-one">
                <label className="address-label">Reportable</label>
                <input
                  type="text/number"
                  className="remediation-section-input"

                  value={_get(this.state.sectionData, [
                    "remediationSection",
                    "reportable"
                  ])}
                ></input>
              </div>{" "}
              <div className="address-two">
                <label className="address-label">CDF Activity Type</label>
                <div className="remediation-field">
                  <select>
                    <option value="volvo">Advertising and Promotion</option>
                    <option value="saab">Charitable Donations</option>
                    <option value="opel">Clinical Trial/Research</option>
                    <option value="audi">Concur</option>
                  </select>
                </div>
              </div>
            </div>
            <div className="address-lines">
              <div className="address-one">
                <label className="address-label">Status</label>
                <div className="remediation-field">
                  <select>
                    <option value="volvo">Complete-Automated</option>
                    <option value="saab">filename</option>
                    <option value="opel">Status</option>
                    <option value="audi">Records</option>
                  </select>
                </div>
              </div>{" "}
              <div className="address-two">
                <label className="address-label">CDF Expense Type</label>
                <div className="remediation-field">
                  <select>
                    <option value="volvo">Advertising and Promotional Materials
</option>

                  </select>
                </div>
              </div>
            </div>
            <div className="address-lines-mdm">
              <div className="address-one-mdm">
                <label className="address-label">MDM ID</label>
                <div className="update-btn">
                  <input
                    type="text/number"
                    name="MDMID"
                    className="remediation-section-input-mdm"
                    onChange={this.inputChange}
                    value={_get(this.state.sectionData, [
                      "remediationSection",
                      "MDMID"
                    ])}
                  ></input>
                  <button className="UPDATE">UPDATE</button>
                </div>
              </div>{" "}
              <div className="address-two">
                <label className="address-label">ToV Amount</label>
                <input
                  type="text/number"
                  className="remediation-section-input"
                  name="ToVAmount"
                  onChange={this.inputChange}
                  value={_get(this.state.sectionData, [
                    "remediationSection",
                    "ToVAmount"
                  ])}
                ></input>
              </div>
            </div>
            <div className="address-lines-mdm">
              <div className="address-one-mdm">
                <label className="address-label">NPI</label>
                <div className="new-label-mdm">
                  <input
                    type="text/number"

                    className={
                      _get(this.state.sectionData, ["remediationSection", "NPI"])
                        ? "remediation-section-input"
                        : "remediation-section-input border-red"
                    }
                    value={_get(this.state.sectionData, [
                      "remediationSection",
                      "NPI"
                    ])}
                  ></input>
                  <div className="reme-checkbox">
                    <input
                      type="checkbox"
                      className="remediation-section-checkbox"
                      value="No MDM"
                    ></input>
                    <label className="no-mdm">No MDM</label>
                  </div>
                </div>
              </div>{" "}
              <div className="address-two">
                <label className="address-label">ToV Currency</label>
                <div className="remediation-field">
                  <select onChange={this.tovCurrency} value={this.state.newCurrency}>
                    <option value="Client MDM">select</option>
                    <option value="USD">USD</option>

                  </select>
                </div>
              </div>
            </div>
            <div className="address-lines">
              <div className="address-one">
                <label className="address-label">MDM Match Confidence</label>
                <div className="remediation-field">
                  <select>
                    <option value="volvo">filename</option>
                    <option value="saab">filename</option>
                    <option value="opel">Status</option>
                    <option value="audi">Records</option>
                  </select>
                </div>
              </div>
              <div className="address-two">
                <label className="address-label">ToV Date</label>
                <input
                  type="date"
                  onChange={this.changeDate}
                  className={
                    _get(this.state.sectionData, ["remediationSection", "TovDate"])
                      ? "remediation-section-input"
                      : "remediation-section-input border-red"
                  }
                  value={this.state.newdate}
                  defaultValue={_get(this.state.sectionData, [
                    "remediationSection",
                    "TovDate"
                  ])}
                ></input>
              </div>
            </div>
            <div className="address-lines">
              <div className="address-one">
                <label className="address-label">First Name</label>
                <input
                  type="text/number"
                  name="FirstName"
                  onChange={this.inputChange}
                  className={
                    _get(this.state.sectionData, ["remediationSection", "FirstName"])
                      ? "remediation-section-input"
                      : "remediation-section-input border-red"
                  }
                  value={_get(this.state.sectionData, [
                    "remediationSection",
                    "FirstName"
                  ])}
                ></input>
              </div>
              <div className="address-two">
                <label className="address-label">
                  Institution Recipient Type
                </label>
                <div className="remediation-field">
                  <select>
                    <option value="volvo">filename</option>
                    <option value="saab">filename</option>
                    <option value="opel">Status</option>
                    <option value="audi">Records</option>
                  </select>
                </div>
              </div>
            </div>
            <div className="address-lines">
              <div className="address-one">
                <label className="address-label">Middle Name</label>
                <input
                  type="text/number"
                  name='MiddleName'
                  onChange={this.inputChange}
                  className={
                    _get(this.state.sectionData, ["remediationSection", "MiddleName"])
                      ? "remediation-section-input"
                      : "remediation-section-input border-red"
                  }
                  value={_get(this.state.sectionData, [
                    "remediationSection",
                    "MiddleName"
                  ])}
                ></input>
              </div>{" "}
              <div className="address-two">
                <label className="address-label">TIN</label>
                <input
                  type="text/number"
                  name='TIN'
                  onChange={this.inputChange}
                  className={
                    _get(this.state.sectionData, ["remediationSection", "TIN"])
                      ? "remediation-section-input"
                      : "remediation-section-input border-red"
                  }
                  value={_get(this.state.sectionData, [
                    "remediationSection",
                    "TIN"
                  ])}
                ></input>
              </div>
            </div>
            <div className="address-lines">
              <div className="name-notes">
                <div className="address-one-notes">
                  <label className="address-label">Last Name</label>
                  <input
                    type="text/number"
                    onChange={this.inputChange}
                    name="LastName"
                    className={
                      _get(this.state.sectionData, ["remediationSection", "LastName"])
                        ? "remediation-section-input"
                        : "remediation-section-input border-red"
                    }
                    value={_get(this.state.sectionData, [
                      "remediationSection",
                      "LastName"
                    ])}
                  ></input>
                </div>
                <div className="address-one-notes">
                  <label className="address-label">Institution Name</label>
                  <input
                    type="text/number"
                    className={
                      _get(this.state.sectionData, ["remediationSection", "institutionName"])
                        ? "remediation-section-input"
                        : "remediation-section-input border-red"
                    }
                    value={_get(this.state.sectionData, [
                      "remediationSection",
                      "institutionName"
                    ])}
                  ></input>
                </div>
                <div className="address-one-notes">
                  <label className="address-label">Recipient Type</label>
                  <div className="remediation-field">
                    <select>
                      <option value="volvo">HCP</option>
                      <option value="saab">HCO</option>

                    </select>
                  </div>
                </div>
              </div>
              <div className="follow-notes">
                <label className="address-label">Follow-up Notes</label>
                <textarea
                  rows="5"
                  cols="60"
                  name="comment"
                  form="usrform"
                  onChange={(note) => { this.setState({ setFollowup: note.target.value }) }}
                ></textarea>
              </div>
              <button className="request-follow" onClick={this.showFollow}>
                REQUEST FOLLOWUP
              </button>
            </div>
            <div className="show-popup-section">
              <Modal
                className="baseline-modal-showcase"
                visible={this.state.visiblePopup}
                onCancel={this.handleCancelPopup}
              >
                <ModalHeader>
                  <div className="section-popup-header">
                    <div className="section-popup-first">
                      <button className="follow-up">FOLLOW UP</button>
                      <h4 className="trans-hub-name">
                        Transparency Hub Record:
                        <span className="rem-record">
                          {localStorage.getItem("sampleFile")}
                        </span>
                      </h4>
                    </div>
                    <h2 className="req-follow-up">Request Follow up</h2>
                  </div>
                </ModalHeader>
                <ModalBody>
                  <div className="section-popup">
                    <div className="section-search">
                      <img className="search" src={Search} alt="Search"></img>
                      <input
                        type="text"
                        placeholder="Type a name or email to request folow up"
                      ></input>
                    </div>
                    <div className="recent-collabartion">
                      <p className="recent-cont">RECENT COLLABORATIONS</p>
                      <div
                        className={
                          this.state.radioValue === "radio1"
                            ? "user-one bg-active"
                            : "user-one"
                        }
                      >
                        <input
                          type="radio"
                          name="radio1"
                          value=""
                          onChange={this.handleRadioChange}
                          checked={
                            this.state.radioValue === "radio1" ? true : false
                          }
                        />
                        <div className="user-left"></div>
                        <div className="user-frank">
                          <div className="user-pic">
                            <img className="frank" alt="" src={Frank}></img>
                          </div>
                          <div className="user-content">
                            <div className="user-supervisor">
                              <span className="frank">Frank Kenney - </span>
                              <p className="user-manager">
                                Transparency Manager
                              </p>
                            </div>
                            <div className="user-mail">
                              <p className="frank-mail">frank.kenny@pwc.com</p>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div
                        className={
                          this.state.radioValue === "radio2"
                            ? "user-one bg-active"
                            : "user-one"
                        }
                      >
                        <input
                          type="radio"
                          name="radio2"
                          value=""
                          onChange={this.handleRadioChange}
                          checked={
                            this.state.radioValue === "radio2" ? true : false
                          }
                        />
                        <div className="user-left"></div>
                        <div className="user-frank">
                          <div className="user-pic">
                            <img className="frank" alt="" src={Kay}></img>
                          </div>
                          <div className="user-content">
                            <div className="user-supervisor">
                              <span className="frank">Kay Totleben - </span>
                              <p className="user-manager">
                                Transparency Manager
                              </p>
                            </div>
                            <div className="user-mail">
                              <p className="frank-mail">kay.totleben@pwc.com</p>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="popup-border"></div>
                      <div className="popup-description">
                        <p className="popup-description-cont">DESCRIPTION</p>
                        <div className="popup-description-content">
                          {this.state.setFollowup}
                        </div>
                      </div>
                      <div className="calender-show">
                        <p className="due">DUE DATE</p>
                        <div className="calender-content">
                          <p className="date-format">DD/MM/YYYY</p>
                          <p className="cal-icon">
                            <i class="fa fa-calendar" aria-hidden="true"></i>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </ModalBody>
                <ModalFooter style={{ justifyContent: "flex-end" }}>
                  <Button class="trans-cancel-one">CANCEL</Button>
                  <NavLink to="/followpopup" className="decorate">
                    <Button class="trans-save-one">REQUEST FOLLOW UP</Button>
                  </NavLink>{" "}
                </ModalFooter>
              </Modal>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default RemediationSection;
