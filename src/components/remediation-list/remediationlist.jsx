import React, { Component } from "react";
import "../remediation-list/remediationlist.scss";
import Stepper from "../remediation-stepper/remediationstepper";
import Breadcrum from "../../common/breadcrumb/breadcrumb";
import { Modal, ModalHeader, ModalBody, ModalFooter } from "appkit-react";

const items = [
  {
    link: "/",
    value: <span className="appkiticon icon-home-fill font-14"></span>
  },
  {
    link: "/remediation",
    value: "Open Tasks"
  },
  {
    link: "/transactionlist",
    value: "Transaction list"
  },
  {
    link: "",
    value: "Remediation list"
  }
];
class RemediationList extends Component {
  constructor() {
    super();
    this.state = {
      initialItems: [],
      items: [],
      visible: false
    };
  }

  showInfoPopup = () => {
    this.setState({
      visible: true
    });
  };

  handleCancel = e => {
    this.setState({
      visible: false
    });
  };
  handleConfirm = e => {
    this.setState({
      visible: false
    });
  };
  selectClient = e => {
    if (e === 2 || e === 3) {
      this.setState({ iconsChangeColor: true });
    }
  };

  render() {
    const containerStyle = { display: "flex", alignItems: "center" };
    const two = (
      <div style={containerStyle}>
        {" "}
        David <div className="a-select-comment"></div>{" "}
      </div>
    );

    const three = (
      <div style={containerStyle}>
        John <div className="a-select-comment"></div>{" "}
      </div>
    );
    return (
      <div className="remediatonlist-wrapper">
        <div className="remediatonlist-content">
          <Modal
            className="baseline-modal-showcase"
            visible={this.state.visible}
            onCancel={this.handleCancel}
          >
            <ModalHeader>
              <div className="reme-out"></div>
            </ModalHeader>
            <ModalBody>
              <div className="reme-popup">
                <div className="reme-name">
                  <h2 className="reme-name-content">Name</h2>
                  <h3 className="reme-name-kiran">Kiran Singh</h3>
                </div>
                <div className="reme-fields">
                  <h2 className="reme-name-content">Fields Edited</h2>
                  <div className="reme-pwc-status">
                    <h3 className="reme-name-kiran">PWC STATUS</h3>
                    <i class="fa fa-sort"></i>
                  </div>
                  <p className="reme-complete">Complete - Automated</p>
                </div>
                <div className="reme-data-modified">
                  <h2 className="reme-name-content">Date of Modification</h2>
                  <div className="reme-pwc-status">
                    <h3 className="reme-name-kiran">PWC STATUS</h3>
                    <i class="fa fa-sort"></i>
                  </div>
                  <p className="reme-complete">Complete - Automated</p>
                </div>
              </div>
            </ModalBody>
            <ModalFooter>
              {/* <Button kind="transparent" onClick={this.handleConfirm}>
                {" "}
                OK{" "}
              </Button> */}
            </ModalFooter>
          </Modal>
          <div className="reme-crud">
            <Breadcrum items={items} />
          </div>
          <div className="remediation-top"></div>
          <div className="remediatonlist-field">
            <div className="content-field">
              <p className="content-field-name">FIELD</p>
              <input type="text" placeholder="PWC Id"></input>
            </div>
            <div className="content-field">
              <p className="content-field-name">OPERATIONS</p>
              <input type="text" placeholder="213"></input>
            </div>
            <div className="content-field">
              <p className="content-field-name">VALUE</p>
              <input type="text" placeholder="Equal to"></input>
            </div>
            <div className="remediatonlist-btns">
              <button className="remediatonlist-add">
                Add Criteria <span className="remediatonlist-plus">+</span>
              </button>
              <button className="remediatonlist-find">FIND</button>
            </div>
          </div>
          <div className="remediation-list">
            <p className="show-result">Showing 1 of 1 results</p>
            <div className="more-btn">
              <i
                class="fa fa-info-circle"
                onClick={this.showInfoPopup}
                aria-hidden="true"
              ></i>
              <i class="fa fa-trash" aria-hidden="true"></i>
            </div>
            <table>
              <tr className="remediation-list-table-head">
                <th className="remediation-list-table">
                  <div className="remediation-list-div">
                    <h2 className="pwc-name">PWC ID</h2>
                    <i className="fa fa-sort"></i>
                  </div>
                </th>
                <th className="remediation-list-table">
                  <div className="remediation-list-div">
                    <h2 className="pwc-name">SOURCE SYSTEM NAME</h2>
                    <i className="fa fa-sort"></i>
                  </div>
                </th>
                <th className="remediation-list-table">
                  <div className="remediation-list-div">
                    <h2 className="pwc-name">PWC STATUS</h2>
                    <i className="fa fa-sort"></i>
                  </div>
                </th>
                <th className="remediation-list-table">
                  <div className="remediation-list-div">
                    <h2 className="pwc-name">PWC REASON CODE</h2>
                    <i className="fa fa-sort"></i>
                  </div>
                </th>
                <th className="remediation-list-table">
                  <div className="remediation-list-div">
                    <h2 className="pwc-name">RECIPIENT TYPE</h2>
                    <i className="fa fa-sort"></i>
                  </div>
                </th>
              </tr>
              <tr className="remediation-list-item">
                <td className="remediation-list-detail">214</td>
                <td className="remediation-list-detail">Appian</td>
                <td className="remediation-list-detail">
                  Complete - Automated
                </td>
                <td className="remediation-list-detail"></td>
                <td className="remediation-list-detail">HCP</td>
              </tr>
              {/* <PaginationList /> */}
            </table>

            <div className="remediation-list-pagination"></div>
          </div>
          <div className="remediation-stepper">
            <h2 className="remediation-stepper-name">
              Transaction data <span className="span-red">(1 Error)</span>
            </h2>
            <div className="remediation-stepper-list">
              <Stepper />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default RemediationList;
