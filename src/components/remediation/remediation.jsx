import React, { Component } from "react";
import Search from "../../images/Search.png";
import "../remediation/remediation.scss";
import { setTimeout } from "timers";
import Loader from "../../common/loader/loader";
import { NavLink } from "react-router-dom";
import axios from "axios";
import PaginationList from "../../common/pagination/pagination";
import Breadcrum from "../../common/breadcrumb/breadcrumb";

const items = [
  {
    link: "/",
    value: <span className="appkiticon icon-home-fill font-14"></span>
  },
  {
    link: "",
    value: "Open Tasks"
  }
];

class Remediation extends Component {
  constructor() {
    super();
    this.state = {
      initialItems: [],
      items: [],
      filter: []
    };
    this.filterList = this.filterList.bind(this);
  }

  componentDidMount() {
    axios.get(`${process.env.REACT_APP_API_URL}/remediation`).then(res => {
      const data = res.data;
      this.setState({ filter: data.data });
      this.setState({ items: data.data });
    });
    this.setState({ items: this.state.filter });
  }

  componentWillMount() {
    setTimeout(() => {
      this.setState({ isLoader: false });
    }, 1000);
    this.setState({
      items: this.state.initialItems,
      isLoader: true
    });
  }

  filterList = e => {
    let total = this.state.filter;
   
    let updateList = total;
    updateList = updateList.filter(item => {
      if (e.target.value !== null) {
        return (
          item.filename.toLowerCase().search(e.target.value.toLowerCase()) !==
          -1
        );
      }
    });
    this.setState({
      items: updateList
    });
  };
  onLinkClick = fileName => {
    localStorage.setItem("sampleFile", fileName);
  };
  getCircleClass(item) {
    return item.status === "In Progress"
      ? "circle"
      : item.status === "Pending Approval"
      ? "done"
      : item.status === "Support Requested"
      ? "restricted"
      : "";
  }

  render() {
    return (
      <React.Fragment>
        {this.state.isLoader ? (
          <Loader />
        ) : (
          <div className="bulk-updates">
            <div className="task-report">
              <Breadcrum items={items} />
              <div className="task-content">
                <div className="report-content">
                  <h2 className="task-name">Open Tasks</h2>
                  <p className="task-number">
                    <span className="bulk-bold">65</span> Projects, Showing{" "}
                    <span className="bulk-bold">12</span>{" "}
                  </p>
                </div>
                <div className="search-bar-bulk">
                  <img className="search" src={Search} alt="Search"></img>
                  <input
                    type="text"
                    placeholder="Search"
                    onChange={e => this.filterList(e)}
                  ></input>
                </div>
              </div>
              <div className="border-bottom-update"></div>
              <div className="filter-projects">
                <h2 className="filter-name">FILTER PROJECTS</h2>
                <div className="dropdown-btns-new">
                  <div className="field-dropdowns">
                    <div className="field">
                      <select>
                        <option value="volvo">Field</option>
                        <option value="saab">filename</option>
                        <option value="opel">Status</option>
                        <option value="audi">Records</option>
                      </select>
                    </div>
                    <div className="metrics-name">
                      <select>
                        <option value="volvo">Operation</option>
                        <option value="saab">Errors</option>
                        <option value="opel">Vendors</option>
                        <option value="audi">Update</option>
                      </select>
                    </div>
                    <div className="input-value">
                      <input placeholder="Value"></input>
                    </div>
                    <button className="update-apply">APPLY</button>
                  </div>
                  <div className="bulk-btns">
                    <button className="bulk">BULK</button>
                    <button className="export">EXPORT</button>
                  </div>
                </div>
              </div>
              <div className="update-table">
                <React.Fragment>
                  <table>
                    <tr>
                      <th className="update-head-new">
                        <div className="head-cont-update">
                          <h4 className="filename">FILENAME</h4>
                          <i class="fa fa-sort"></i>
                        </div>
                      </th>
                      <th className="update-head-new">
                        <div className="head-cont-update">
                          <h4 className="filename">STATUS</h4>
                          <i class="fa fa-sort"></i>
                        </div>
                      </th>
                      <th className="update-head-new">
                        <div className="head-cont-update">
                          <h4 className="filename">RECORDS</h4>
                          <i class="fa fa-sort"></i>
                        </div>
                      </th>
                      <th className="update-head-new">
                        <div className="head-cont-update">
                          <h4 className="filename">ERRORS</h4>
                          <i class="fa fa-sort"></i>
                        </div>
                      </th>
                      <th className="update-head-new">
                        <div className="head-cont-update">
                          <h4 className="filename">LAST UPDATE</h4>
                          <i class="fa fa-sort"></i>
                        </div>
                      </th>
                      <th className="update-head-new">
                        <div className="head-cont-update">
                          <h4 className="filename">VENDOR NAME</h4>
                          <i class="fa fa-sort"></i>
                        </div>
                      </th>
                    </tr>
                    {this.state.items.map((item, index) => {
                      return (
                        <tr key={item.FileName}>
                          <td className="row-content">
                            <NavLink
                              to={{
                                pathname: "/transactionlist",
                                state: "fromTransactionList"
                              }}
                              className="decorate"
                              onClick={() => this.onLinkClick(item.filename)}
                            >
                              {item.filename}
                            </NavLink>
                          </td>
                          <td className="row-cont-circle">
                            <div
                              className="circle"
                              className={this.getCircleClass(item)}
                            ></div>
                            <div className="status-name">{item.status}</div>
                          </td>
                          <td className="row-cont"> {item.records}</td>
                          <td className="row-cont">{item.errors}</td>
                          <td className="row-cont"> {item.lastupdate}</td>
                          <td className="row-cont">{item.vendorname}</td>
                          <td className="row-edit">
                            <NavLink to="/transactionlist" className="decorate">
                              <i class="fa fa-edit"></i>
                            </NavLink>
                          </td>
                          <td className="row-delete">
                            <i class="fa fa-trash"></i>
                          </td>
                        </tr>
                      );
                    })}
                  </table>
                  <PaginationList />
                </React.Fragment>
              </div>
            </div>
          </div>
        )}
      </React.Fragment>
    );
  }
}
export default Remediation;
