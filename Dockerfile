FROM node:10.16.0-alpine

RUN mkdir -p /app && mkdir -p /run/nginx/ && \
        mkdir -p /var/www/html

WORKDIR /app

RUN apk update && apk add --no-cache nginx && \
    rm -rf /var/cache/apk/* && \
    chown -R nginx:nginx /var/www/html && \
    chown -R nginx:nginx /run/nginx

COPY . /app

RUN cp nginx.conf /etc/nginx/conf.d/default.conf

#RUN npm install
#RUN npm run build

RUN apk update && apk add curl
RUN curl -u rashika.r.agarwal@in.pwc.com:AKCp5e3yNna6yG5KkERwVqnCCxTHVqxBTMZnsVuSAUMHYa6tKuurkkxTcDprh1K5KVsfeU8HV https://artifacts.pwc.com/artifactory/api/npm/auth >> ~/.npmrc
RUN npm install --registry=https://artifacts.pwc.com/artifactory/api/npm/us-adv-digital-npm/ appkit --save
RUN npm run build
RUN cp -rf ./build/ /var/www/html/build && \
    chown -R nginx:nginx /var/www/html/build

#RUN mkdir -p /etc/nginx/certs/

#RUN cp fullchain.pem /etc/nginx/certs/fullchain.pem

#RUN cp privkey.pem /etc/nginx/certs/privkey.pem

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
#CMD ["sh","-c","tail -f /dev/null"]